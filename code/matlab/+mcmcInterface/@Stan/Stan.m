classdef Stan < mcmcInterface.samplerInterfaces
    %STAN
    %   Custom samplerInterfaces child class for HMC sampling via Stan (https://mc-stan.org/).
    
    properties (Constant, Access = private) %define as private so that subclasses can have their own instances with same name
        cSTARTUPDELAY=10;      % startup delay for gnome-terminal and os-agnostic-utils
    end

    properties
        cmdStanDir          % directory to the source code for CmdStan (https://mc-stan.org/users/interfaces/cmdstan)
        cmdStanShortCut     % shortcut to the CmdStan executable that is built from the source code and used to compile Stan project files
        adaptDelta          % target average proposal acceptance probability during adaptation period (Stan only)
    end
    
    methods (Static)
        function setStan=readStructFromXMLNode(rootNode, myrepos)     
            %readStructFromXMLNode
            %   Worker function that parses and returns information within a structure that represents a root element.  Function can also be called
            %with a sparse structure, that only contains some (and not all) fields in the root element (so this function must handle missing entries).
            % rootNode-data structure with the xml node read in via xmlread
            % myrepos (optional)-myrepos object with base dir and list of repositories
            
            for i=1:rootNode.getLength()
                tNode=rootNode.item(i-1);
                if (strcmp(tNode.getNodeName,'dir')==1)
                    tDir=mcmcInterface.storageObjectPath(tNode,myrepos);
                    if (strcmp(tDir.name,'cmdStanDir')==1)
                        setStan.cmdStanDir=tDir.path;
                    else
                        error('unexpected xml node name encountered for dir');
                    end
                elseif (strcmp(tNode.getNodeName,'file')==1)
                    tFile=mcmcInterface.storageObjectPath(tNode,myrepos);
                    if (strcmp(tFile.name,'cmdStanShortCut')==1)
                        setStan.cmdStanShortCut=tFile.path;
                    else
                        error('unexpected xml node name encountered for dir');
                    end                    
                elseif (strcmp(tNode.getNodeName,'adaptDelta')==1)
                    setStan.adaptDelta=str2num(cast(tNode.getTextContent(),'char'));
                end
            end
        end
    end

    methods% (Access=protected)
        function buildCmdStan(obj)
            stdout=1;           
            fprintf(stdout,'Compiling CmdStan binary\n');
            tempscript=mcmcInterface.osInterfaces.convPathStr(strcat(obj.hOsInterfaces.dirTemp,mcmcInterface.osInterfaces.getNativeSeparator(),'buildCmdStanScript.txt'));    
            templog=mcmcInterface.osInterfaces.convPathStr(strcat(obj.hOsInterfaces.dirLogs,mcmcInterface.osInterfaces.getNativeSeparator(),'buildCmdStanLog.txt'));     
            fid=fopen(tempscript,'w');%don't use 't' (for text) so that we can control line endings to be unix            
            fprintf(fid,'#!/usr/bin/env bash\n echo sleeping %d seconds to allow os-agnostic-utils time to fork a process and determine its pid\n',obj.cSTARTUPDELAY);
            fprintf(fid,'sleep %i\n',obj.cSTARTUPDELAY);
            switch obj.hOsInterfaces.type
                case {mcmcInterface.t_osInterfaces.WindowsOS}                               
                    fprintf(fid,'cd %s\n',mcmcInterface.osInterfaces.convPathStrWSLUnix(obj.cmdStanDir));
                case {mcmcInterface.t_osInterfaces.MacOS,mcmcInterface.t_osInterfaces.LinuxOS}
                    fprintf(fid,'cd %s\n',mcmcInterface.osInterfaces.convPathStrUnix(obj.cmdStanDir));                    
            end
            fprintf(fid,'make clean-all\n');
            fprintf(fid,'make build -j4\n');
            fprintf(fid,'make bin/diagnose\n');
            if obj.hOsInterfaces.type==mcmcInterface.t_osInterfaces.LinuxOS || obj.hOsInterfaces.type==mcmcInterface.t_osInterfaces.MacOS
                syscommand=sprintf('system(''chmod +x "%s"'');',tempscript);%make the script executable
                eval(syscommand);                
            end  
            fclose(fid);            

            compileTimerStart=tic;
            %needs to run and wait, as otherwise, *.exe appears prior to completion of entire build
            obj.hOsInterfaces.spawnBashCommand( tempscript, templog, mcmcInterface.t_runView.Foreground);
            finished=false;

            setcommandstr='';
            while finished==false
                drawnow;                
                pause(1);%sleep 1 seconds
                elapsedtime=toc(compileTimerStart);
                durelapsedseconds=seconds(elapsedtime);
                elapsedminutes=minutes(durelapsedseconds);
                statusstr=sprintf('Duration for building CmdStan is %e', elapsedminutes);
                setDesktopStatus(statusstr)
                [retcommandstr]=obj.hOsInterfaces.updateBashCmdStatus(setcommandstr);
                setcommandstr=retcommandstr;
                %disp(setcommandstr);
                obj.hOsInterfaces.checkForKillFile();
                if (obj.hOsInterfaces.bashCmdState==mcmcInterface.t_runState.Finished)
                    finished=true;
                    tempstr=sprintf('\tTotal build time for CmdStan was %e', elapsedminutes);
                    disp(tempstr);    
                    setDesktopStatus(tempstr);
                end
            end
        end
        
        function buildStanProject(obj)
            stdout=1;
            fprintf(stdout,'Compiling stan binary\n');
            projectFileName=strcat(obj.dirProject,mcmcInterface.osInterfaces.getNativeSeparator(),obj.fileProject,'.stan');
            hppname=strrep(projectFileName,'.stan','.hpp');
            switch obj.hOsInterfaces.type
                case {mcmcInterface.t_osInterfaces.WindowsOS}
                    binname=strrep(projectFileName,'.stan','');
                    tempscript=mcmcInterface.osInterfaces.convPathStr(strcat(obj.hOsInterfaces.dirTemp, mcmcInterface.osInterfaces.getNativeSeparator(), obj.fileProject,'-buildStanScript.txt'));    
                    templog=mcmcInterface.osInterfaces.convPathStr(strcat(obj.hOsInterfaces.dirTemp, mcmcInterface.osInterfaces.getNativeSeparator(), obj.fileProject,'-buildStanLog.txt'));     
                case {mcmcInterface.t_osInterfaces.LinuxOS,mcmcInterface.t_osInterfaces.MacOS}
                    binname=strrep(projectFileName,'.stan','');
                    tempscript=mcmcInterface.osInterfaces.convPathStrUnix(strcat(obj.hOsInterfaces.dirTemp, mcmcInterface.osInterfaces.getNativeSeparator(), obj.fileProject,'-buildStanScript.txt'));    
                    templog=mcmcInterface.osInterfaces.convPathStrUnix(strcat(obj.hOsInterfaces.dirTemp, mcmcInterface.osInterfaces.getNativeSeparator(), obj.fileProject,'-buildStanLog.txt'));     
            end
            
            fid=fopen(tempscript,'w');%don't use 't' (for text) so that we can control line endings to be unix            
            fprintf(fid,'#!/usr/bin/env bash\n echo sleeping %d seconds to allow os-agnostic-utils time to fork a process and determine its pid\n',obj.cSTARTUPDELAY);
            fprintf(fid,'sleep %i\n',obj.cSTARTUPDELAY);
            switch obj.hOsInterfaces.type
                case {mcmcInterface.t_osInterfaces.WindowsOS}
                    fprintf(stdout,'Enabling stanc optimizations with --O\n');
                    fprintf(fid,'cd %s\n',mcmcInterface.osInterfaces.convPathStrWSLUnix(obj.cmdStanDir));
                    fprintf(fid,'%s --O --name=%s --o=%s %s\n', mcmcInterface.osInterfaces.convPathStrWSLUnix(obj.cmdStanShortCut), obj.fileProject, mcmcInterface.osInterfaces.convPathStrWSLUnix(hppname), mcmcInterface.osInterfaces.convPathStrWSLUnix(projectFileName));
                    fprintf(fid,'make %s\n',mcmcInterface.osInterfaces.convPathStrWSLUnix(binname));       
                case {mcmcInterface.t_osInterfaces.LinuxOS,mcmcInterface.t_osInterfaces.MacOS}
                    fprintf(fid,'cd %s\n',mcmcInterface.osInterfaces.convPathStrUnix(obj.cmdStanDir));
                    fprintf(fid,'%s --O --name=%s --o=%s %s\n', mcmcInterface.osInterfaces.convPathStrUnix(obj.cmdStanShortCut), obj.fileProject, mcmcInterface.osInterfaces.convPathStrUnix(hppname), mcmcInterface.osInterfaces.convPathStrUnix(projectFileName));
                    fprintf(fid,'make %s\n',mcmcInterface.osInterfaces.convPathStrUnix(binname));       
            end
            fclose(fid);
            if obj.hOsInterfaces.type==mcmcInterface.t_osInterfaces.LinuxOS || obj.hOsInterfaces.type==mcmcInterface.t_osInterfaces.MacOS
                syscommand=sprintf('system(''chmod +x %s'');',mcmcInterface.osInterfaces.convPathStrUnix(tempscript));%make the script executable
                eval(syscommand); 
            end  
            
            compileTimerStart=tic;
            %needs to run and wait, as otherwise, *.exe appears prior to completion of entire build
            obj.hOsInterfaces.spawnBashCommand(tempscript, templog, mcmcInterface.t_runView.Foreground);
            
            finished=false;

            setcommandstr='';
            while finished==false
                drawnow;                
                pause(1);%sleep 1 seconds
                elapsedtime=toc(compileTimerStart);
                durelapsedseconds=seconds(elapsedtime);
                elapsedminutes=minutes(durelapsedseconds);
                statusstr=sprintf('Stan compile time for %s is %e', obj.fileProject, elapsedminutes);
                setDesktopStatus(statusstr)
                retcommandstr=obj.hOsInterfaces.updateBashCmdStatus(setcommandstr);
                setcommandstr=retcommandstr;
                %disp(setcommandstr);
                obj.hOsInterfaces.checkForKillFile();
                if (obj.hOsInterfaces.bashCmdState==mcmcInterface.t_runState.Finished)
                    finished=true;
                    tempstr=sprintf('\tTotal Stan compile time for %s was %e', obj.fileProject, elapsedminutes);
                    disp(tempstr);    
                    setDesktopStatus(tempstr);
                end
            end
        end
    end
    
    methods
        function obj = Stan(setStan,setOsInterface,setDebug,setDirSampler,setDirProject,setFileProject,setDirScript,setFileScript,...
                        setNumMaxAdaptationSteps,setNumBurnInSteps,setNumLagSteps,setNumSamplingSteps,setNumSamplingChains,...
                        setDirSamplerInterfaces,setFileSamplerInterfaces)
            %STAN Construct an instance of this class
            %   Detailed explanation goes here
            if nargin == 0
                superargs={};
            else
                superargs{1}=mcmcInterface.t_samplerInterfaces.Stan;
                superargs{2}=setOsInterface;
                superargs{3}=setDebug;
                superargs{4}=setDirSampler;
                superargs{5}=setDirProject;
                superargs{6}=setFileProject;
                superargs{7}=setDirScript;
                superargs{8}=setFileScript;
                superargs{9}=setNumMaxAdaptationSteps;
                superargs{10}=setNumBurnInSteps;
                superargs{11}=setNumLagSteps;
                superargs{12}=setNumSamplingSteps;
                superargs{13}=setNumSamplingChains;
                superargs{14}=setDirSamplerInterfaces;
                superargs{15}=setFileSamplerInterfaces;
            end

            obj=obj@mcmcInterface.samplerInterfaces(superargs{:});
            
            obj.cmdStanDir=setStan.cmdStanDir;
            obj.cmdStanShortCut=setStan.cmdStanShortCut;
            obj.adaptDelta=setStan.adaptDelta;
        end               

        % mcmcInterface.samplerInterfaces class declares this function as abstract and its children must implement it
        function setup(obj, hOsInterfaces, uuid, fileNamePrefix, variables, data)
            obj.hOsInterfaces=hOsInterfaces;
            
            %dirRefScript and fileRefScript are not used for Stan
            
            obj.fileBashScript=strcat(obj.hOsInterfaces.dirTemp,obj.hOsInterfaces.getNativeSeparator(),fileNamePrefix,'-',uuid,'-bashScript.txt');
            obj.fileScript=strcat(obj.hOsInterfaces.dirTemp,mcmcInterface.osInterfaces.getNativeSeparator(),fileNamePrefix,'-',uuid,'-script.txt');
            obj.fileInits=strcat(obj.dirSampler,mcmcInterface.osInterfaces.getNativeSeparator(),'inits',mcmcInterface.osInterfaces.getNativeSeparator(),fileNamePrefix,'-', uuid,'-inits.txt');
            obj.fileData=strcat(obj.dirSampler,mcmcInterface.osInterfaces.getNativeSeparator(),'input',mcmcInterface.osInterfaces.getNativeSeparator(),fileNamePrefix,'-', uuid,'-data.txt');
                        
            obj.setupBashScript();
            obj.setupInits(variables);
            obj.setupData(variables, data);     
            
            if (obj.debug==true)
                obj.numBurnInSteps=obj.numBurnInSteps/10;
                %warning('For Debug mode, reducing numBurnInSteps to %d.',obj.numBurnInSteps);
                obj.numSamplingSteps=obj.numSamplingSteps/10;
                %warning('For Debug mode, reducing numSamplingSteps to %d.',obj.numSamplingSteps);
            end
        end

        % mcmcInterface.samplerInterfaces class declares this function as abstract and its children must implement it        
        function run(obj, uuid, fileNamePrefix, runView)
            %METHOD1 Summary of this method goes here
            %   Detailed explanation goes here
            
            stdout=1;
            %Run the script to generate the MCMC computations for theta(i|j) (cannot run Stan in the background as the next loop iteration can't start without results)
            for chainID=1:obj.numSamplingChains
                fprintf(stdout, '\tStarting Stan sampling for uuid=%s, chainID=%d.\n', uuid, chainID);
                %setup obj.filename_Stanscript for sampling sequence
                obj.fileOutput=strcat(obj.dirSampler,mcmcInterface.osInterfaces.getNativeSeparator(),'output',mcmcInterface.osInterfaces.getNativeSeparator(),fileNamePrefix,'-',uuid,'-chainid',num2str(chainID),'-samples.txt');
                logFileName=strcat(obj.hOsInterfaces.dirLogs,mcmcInterface.osInterfaces.getNativeSeparator(),fileNamePrefix,'-',uuid,'-chainid',num2str(chainID),'-log.txt');

                setupSamplerScript(obj);              
               
                if (mcmcInterface.osInterfaces.doesFileExist(logFileName))%remove any old log files (with the same name)
                    delete(logFileName);
                end
                if (mcmcInterface.osInterfaces.doesFileExist(obj.fileOutput)) %remove any old output files (with the same name)
                    delete(obj.fileOutput);
                end                               

                obj.hOsInterfaces.spawnBashCommand( obj.fileBashScript, logFileName, runView);

                finished=false;
                samplingTimerStart=tic;
                setcommandstr='';
                while finished==false
                    drawnow;
                    pause(3);%sleep 3 seconds
                    elapsedtime=toc(samplingTimerStart);
                    elapsedseconds=seconds(elapsedtime);
                    elapsedminutes=minutes(elapsedseconds);
                    statusstr=sprintf('For project=%s, chainID=%d, Stan sampler elapsed time is %e minutes', obj.fileProject, chainID, elapsedminutes);
                    setDesktopStatus(statusstr)
                    [retcommandstr]=obj.hOsInterfaces.updateBashCmdStatus(setcommandstr);
                    setcommandstr=retcommandstr;
                    obj.hOsInterfaces.checkForKillFile();               
                    %CmdStan output file update timing is wonky, check for the output file being static first and then wait for last line containing '#  Elapsed Time:'
                    if (obj.hOsInterfaces.bashCmdState==mcmcInterface.t_runState.Finished)
                        for count=1:12%will be about 12*(5) seconds
                            if checkFileStatic(obj.fileOutput,5)
                                break;%file exists and is not changing
                            end
                            drawnow;
                            pause(1);
                        end
                        if count==12
                            type(logFileName);
                            error('Sampler failed.');   
                        else%stan sometimes finishes writing slowly
                            filecontents=fileread(obj.fileOutput);
                            foundstring=strfind(filecontents,'#  Elapsed Time:');
                            while (isempty(foundstring))
                                drawnow;
                                pause(1);
                                filecontents=fileread(obj.fileOutput);
                                foundstring=strfind(filecontents,'#  Elapsed Time:');
                            end
                         end
                        finished=true;
                        tempstr=sprintf('\tElapsed sampling time for project=%s, chainID=%d was %e minutes.', obj.fileProject, ...
                            chainID, elapsedminutes);
                        disp(tempstr);    
                        setDesktopStatus(tempstr);
                        obj.hOsInterfaces.deleteCmdMsgFile()                        
                    end
                end                        
            end
        end       

        % mcmcInterface.samplerInterfaces class declares this function as abstract and its children must implement it        
        function retSamples = read(obj, uuid, fileNamePrefix, variables, samples, chainid)
            %READ Summary of this method goes here
            %   updates obj.samples
            
            retSamples=samples(chainid);
            retSamples.uuid=uuid;
            retSamples.chainid=chainid;
            
            samplerOutputFileName=strcat(obj.dirSampler,mcmcInterface.osInterfaces.getNativeSeparator(),'output',mcmcInterface.osInterfaces.getNativeSeparator(),fileNamePrefix,'-',uuid,'-chainid',num2str(chainid),'-samples.txt');

            %otherwise, trim chainid%dsamples.txt and repeat with the current chainid%d
            endloc=strfind(samplerOutputFileName,'chainid');
            tempstr=samplerOutputFileName(1:endloc-1);
            tsamplerOutputFileName=strcat(tempstr,'chainid',num2str(chainid),'-tsamples.txt');
            try
                %create a temporary copy of the file that will be edited in mcmcInterface.stan2mat
                copyfile(samplerOutputFileName,tsamplerOutputFileName);
            catch
                error('You are likely running with mcmcInterface.simulation.sampleMcmcResults=False and no preexisting data.');
            end
            Standatastruct=mcmcInterface.stan2mat(variables,tsamplerOutputFileName);
            names=fieldnames(Standatastruct);%need to identify the fields present in the coda 

            %delete the temporary file tsamplerOutputFileName
            delete(tsamplerOutputFileName);
                          
            clear tfieldindices targlist;
            for varid=1:size(variables.elements,1)
                tsize=obj.numSamplingSteps/obj.numLagSteps;
                if variables.elements(varid).distribution.type==mcmcInterface.t_elementDistribution.fixed
                    tvar=variables.elements(varid).values.valueConstant.data;
                    if exist('tfieldindices','var')==1
                        tfieldindices=[tfieldindices; varid];
                        targlist=[targlist; {tvar}];
                    else
                        tfieldindices=varid;
                        targlist=[{tvar}];
                    end
                else
                    for i=1:numel(names)
                        if strcmp(variables.elements(varid).name,cell2mat(names(i)))==1
                            evalstr=sprintf('Standatastruct.%s',variables.elements(varid).name);
                            tvar=eval(evalstr);
                            if exist('tfieldindices','var')==1 %is this not the first entry (i.e., vector already created)
                                tfieldindices=[tfieldindices; varid];
                                targlist=[targlist; {tvar}];
                            else
                                tfieldindices=varid;
                                targlist=[{tvar}];
                            end
                            break;
                        end
                    end
                    retSamples.elements(varid).values.valueGenerated.data=tvar;
                end
            end               
        end

        function setupSamplerScript(obj)
            %Using a script file to spawn a sampling sequence
            %Initializes the script file to run the project file binary with appropriate arguments
            stdout=1;           
            switch obj.hOsInterfaces.type
                case {mcmcInterface.t_osInterfaces.WindowsOS}
                    samplerProjectFileName=strcat(obj.dirProject,mcmcInterface.osInterfaces.getNativeSeparator(),obj.fileProject);
                case {mcmcInterface.t_osInterfaces.LinuxOS,mcmcInterface.t_osInterfaces.MacOS}
                    samplerProjectFileName=strcat(obj.dirProject,mcmcInterface.osInterfaces.getNativeSeparator(),obj.fileProject);
            end            
            %does samplerProjectFileName exist
            if ~(mcmcInterface.osInterfaces.doesFileExist(samplerProjectFileName))
                obj.buildStanProject;
            end
            
            if (mcmcInterface.osInterfaces.doesFileExist(obj.fileScript)) %remove any old script files (with the same name)
                delete(obj.fileScript);
            end       
            
            %write output to obj.samplerScriptFileName
            fid=fopen(obj.fileScript,'w');%don't use 't' (for text) so that we can control line endings to be unix
            
            %necessary to prevent os-agnostic-utils os_processes issues to query pid with quickly finishing sampling sessions
            fprintf(fid,'#!/usr/bin/env bash\n echo sleeping %i seconds to allow os-agnostic-utils time to fork a process and determine its pid\n',obj.cSTARTUPDELAY);
            fprintf(fid,'sleep %i\n',obj.cSTARTUPDELAY);
            %sample num_samples=%d, num_warmup=%d, thin=%d
            switch obj.hOsInterfaces.type
                case {mcmcInterface.t_osInterfaces.WindowsOS}
                    fprintf(fid,'%s sample num_samples=%d num_warmup=%d thin=%d adapt delta=0.99 data file=%s init=%s output file=%s\n', ...
                        mcmcInterface.osInterfaces.convPathStrWSLUnix(samplerProjectFileName), ...
                        obj.numSamplingSteps, ...
                        obj.numBurnInSteps, ...
                        obj.numLagSteps, ...
                        mcmcInterface.osInterfaces.convPathStrWSLUnix(obj.fileData), mcmcInterface.osInterfaces.convPathStrWSLUnix(obj.fileInits), ...
                        mcmcInterface.osInterfaces.convPathStrWSLUnix(obj.fileOutput));
                    %run diagnostics
                    fprintf(fid,'%s/bin/diagnose %s\n',mcmcInterface.osInterfaces.convPathStrWSLUnix(obj.cmdStanDir),mcmcInterface.osInterfaces.convPathStrWSLUnix(obj.fileOutput));
                case {mcmcInterface.t_osInterfaces.LinuxOS,mcmcInterface.t_osInterfaces.MacOS}
                    fprintf(fid,'%s sample num_samples=%d num_warmup=%d thin=%d adapt delta=0.99 data file=%s init=%s output file=%s\n', ...
                        mcmcInterface.osInterfaces.convPathStrUnix(samplerProjectFileName), ...
                        obj.numSamplingSteps, ...
                        obj.numBurnInSteps, ...
                        obj.numLagSteps, ...
                        mcmcInterface.osInterfaces.convPathStrUnix(obj.fileData), mcmcInterface.osInterfaces.convPathStrUnix(obj.fileInits), ...
                        mcmcInterface.osInterfaces.convPathStrUnix(obj.fileOutput));
                    %run diagnostics
                    fprintf(fid,'%s/bin/diagnose %s\n',mcmcInterface.osInterfaces.convPathStrUnix(obj.cmdStanDir),mcmcInterface.osInterfaces.convPathStrUnix(obj.fileOutput));
            end
            fclose(fid);
            %fprintf(stdout,'created %s\n',obj.filename_Stanscript);
            if obj.hOsInterfaces.type==mcmcInterface.t_osInterfaces.LinuxOS || obj.hOsInterfaces.type==mcmcInterface.t_osInterfaces.MacOS
                syscommand=sprintf('system(''chmod +x "%s"'');',obj.fileScript);%make the script executable
                %disp(strcat('running: ',syscommand));
                eval(syscommand);
            end   
        end    
    end
    
    methods %getter/setter functions
        function set.cmdStanShortCut(obj,setcmdStanShortCut)
            if mcmcInterface.osInterfaces.doesFileExist(setcmdStanShortCut)
                obj.cmdStanShortCut=setcmdStanShortCut;
            else
                obj.buildCmdStan();
                mcmcInterface.osInterfaces.waitFileExists_timeout(setcmdStanShortCut,60);
                obj.cmdStanShortCut=setcmdStanShortCut;
            end
        end
    end  
end

