classdef element
    %ELEMENT 
    %   This general data structure is used to represent model constants, variables, data, samples, and statistics on samples.
    
    properties
        uuid                % hierarchical, unique identifier for the element
        name                % name for the element
        description         % description of the element
        group               % model specific group that associates related variables and data into common sets
        distribution        % stores information describing the stochastic nature of the element
        values              % values object with the data, variables, or samples information
        statistics          % summary statistics on samples (e.g., mean, median, sd, min/max, hpd, mce)
    end
    
    methods (Static)
        function setElement = readStructFromXMLNode(elementNode)
            %stdout=1;
            %fprintf(stdout, '\tLoading the element from elementNode\n');

            setElement.uuid=cast(elementNode.getAttribute('uuid'),'char');
            setElement.name=cast(elementNode.getAttribute('name'),'char');
            setElement.description=cast(elementNode.getAttribute('description'),'char');
            setElement.group=cast(elementNode.getAttribute('group'),'char');
            
            tList=elementNode.getElementsByTagName('distribution');
            distNode=tList.item(0);
            if ~isempty(distNode)
                setElement.distribution=mcmcInterface.distribution.readStructFromXMLNode(distNode);
            end
            
            tList=elementNode.getElementsByTagName('values');
            valueNode=tList.item(0);
            if ~isempty(valueNode)
                setElement.values=mcmcInterface.values.readStructFromXMLNode(valueNode);
            end

            tList=elementNode.getElementsByTagName('statistics');
            statisticsNode=tList.item(0);
            if ~isempty(statisticsNode)                
                setElement.statistics = mcmcInterface.statistics.readStructFromXMLNode(statisticsNode);
            end
        end        
    
        function retElement=constructor(setElement)
            %stdout=1;
    		%fprintf(stdout, 'Constructing the element object\n');
            tdistribution=mcmcInterface.distribution.constructor(setElement.distribution);
            tvalues=mcmcInterface.values.constructor(setElement.values);            
            fieldnameslist=fieldnames(setElement);
            if(~any(contains(fieldnameslist,'statistics')))
                tstatistics=mcmcInterface.statistics.empty();
            else
                tstatistics=mcmcInterface.statistics.constructor(setElement.statistics);
            end
            
            retElement=mcmcInterface.element(setElement.uuid,setElement.name,setElement.description,setElement.group,tdistribution,tvalues,tstatistics);
        end                
    end
    
    methods
        function obj = element(setUuid,setName,setDescription,setGroup,setDistribution,setValues,setStatistics)
            %ELEMENT Construct an instance of this class
            if nargin > 0               
                obj.uuid=setUuid;
                obj.name=setName;
                obj.description=setDescription;
                obj.group=setGroup;
                obj.distribution=setDistribution;
                obj.values=setValues;
                obj.statistics=setStatistics;
            end
        end

        function quickLook(obj, limitValues)
            %limit (optional)-when true, only print first 100 values
            stdout=1;

            valueStr=obj.values.valueConstant.toString();
            if exist('limitValues','var')
                if limitValues==true
                    if size(valueStr,2)>100
                        maxChar=100;
                    else
                        maxChar=size(valueStr,2);
                    end
                    if maxChar>0
                        valueStr=valueStr(1,1:maxChar);
                    end
                end
            end

            if obj.distribution.type==mcmcInterface.t_elementDistribution.fixed
                fprintf(stdout, '%s=%s\n',obj.name, valueStr)
            else
                fprintf(stdout, '%s=%s, initial=%s\n',obj.name, obj.distribution.expression, valueStr)
            end
        end

        function retNode = createXmlNodeFromObject(obj,docNode)
        %     <element id="1" 
        % 		 name="beta0" 
        % 		 description="y intercept variable for linear regression"
        % 		 group="NULL">
        % 	      <distribution type="standard">normal(a,b)</distribution>
        % 	      <values dimensions="scalar" 
        % 			    type="double"
        % 			    lowerBound=""
        % 			    upperBound=""
        % 			    precision="1.0E-1">
        % 		        <constant>[0]</constant><!--initial value-->
        %               <generated>[]</generated><!--samples-->
        % 	      </values>
        %         <statistics><!--statistics on the samples-->
        %             <mean>[]</mean>
        %             <median>[]</median>
        %             <sd>[]</sd>
        %             <min>[]</min>
        %             <max>[]</max>
        %             <hdrlo>[]</hdrlo>
        %             <hdrhi>[]</hdrhi>
        %             <mce>[]</mce>
        %         </statistics>
        %     </element>
            retNode=docNode.createElement('element');
            retNode.setAttribute('uuid',obj.uuid);
            retNode.setAttribute('name',obj.name);
            retNode.setAttribute('description',obj.description);
            retNode.setAttribute('group',obj.group);

            tNode=obj.distribution.createXmlNodeFromObject(docNode);
            retNode.appendChild(tNode);

            tNode=obj.values.createXmlNodeFromObject(docNode);
            retNode.appendChild(tNode);

            tNode=obj.statistics.createXmlNodeFromObject(docNode, obj.values.dimensions);
            retNode.appendChild(tNode);
        end
    end

    methods %getter/setter functions
        function obj=set.distribution(obj, setDistribution)
            if ~(isa(setDistribution,'mcmcInterface.distribution'))
                error('setDistribution is not mcmcInterface.distribution object');
            end
            obj.distribution=setDistribution;
        end
       
        function obj=set.values(obj, setValues)
            if ~(isa(setValues,'mcmcInterface.values'))
                error('setValues is not mcmcInterface.values object');
            end
            obj.values=setValues;
        end
        
        function obj=set.statistics(obj, setStatistics)
            if ~(isa(setStatistics,'mcmcInterface.statistics'))
                error('setStatistics is not mcmcInterface.statistics object');
            end
            obj.statistics=setStatistics;
        end

    end      
end

