classdef osInterfaces < handle
    %OSINTERFACES 
    %   This extensible class, mcmcInterface.osInterfaces, contains functions and a data structure with information 
    % related to features in the operating system being used.  One of the main features of this class is to spawn 
    % the external executables that perform the MCMC or HMC sampling.
    %
    % This is a class that is used from multiple classes (e.g., mcmcInterface.simulation and 
    % mcmcInterface.samplerInterfaces).  To avoid having multiple copies of MATLAB value classes (default type for 
    % classes in MATLAB), it is instead instantiated as MATLAB handle class.  MATLAB handle classes define objects 
    % that reference the object. For these, copying an object creates another reference to the same object.

    properties (Constant, Access = private) %define as private so that subclasses can have their own instances with same name
        version='1.0';      % API version for this class
        cTIMEOUT=300        % timeout of 300 seconds should be large enough for any non error condition (e.g., accomodates lengthy load time for a large data file)
    end

    properties
        type                % type of the osInterfaces object (mcmcInterface.t_osInterfaces enum)

        dirOsInterfaces     % directory that contains osInterfaces *.xml file to load
        fileOsInterfaces    % filename of the osInterfaces *.xml file to load

        dirTemp             % directory for the temporary files created when running simulations and where kill file should be placed to force halt simulations
        dirLogs             % directory for the log files created when running simulations
        
        fileKill            % file name for the kill file (its presence will force terminate any long running command)

        python              % path to python executable
        pythonUtilsPackage  % python package with os agnostic utilities for commanding/controlling bash scripts
        javac               % path to javac executable        
        
        bashCmdUuid         % unique identifier for currently running bash command
        bashCmdMsgFile      % message file between pythonUtilsPackage and MATLAB for currently running bash command
        bashCmdLogFile      % log file for currently running bash command
        bashCmdPid          % PID of currently running bash command
        bashCmdName         % command name of currently running bash command
        bashCmdLine         % command line for currently running bash command 
        bashCmdState        % state of currently running bash command
        bashCmdExitCode     % exit code of most recently completed command   
    end
    
    methods (Static)
        function setOsInterfaces=readStructFromXMLFile(xmlfile,myrepos)        
            %readStructFromXMLFile 
            %   Load a simulation object from *.xml.
            % xmlfile-simulation xmlfile name (either absolute or relative path inside repos's config directory)
            % myrepos (optional)-myrepos object with base dir and list of repositories
            stdout=1;
            fprintf(stdout, 'Loading %s for the osInterfaces\n',xmlfile);
            
            doc = xmlread(xmlfile);
            %fprintf(1,'Parsed %s\n',xmlfile);
            %javaaddpath(mcmcBayes.osInterfaces.getPath(mcmcBayes.t_paths.javaDir));
            %call our java hacks method to insert the DOCTYPE element
            entityname='osInterfaces';
            %     xmlStr=XmlHacks.insert(doc, entityname, dtdfile);%call Java function  (XmlHacks must be on javaclasspath)
            %     %now parse the hacked xml
            %     doc = XmlHacks.xmlreadstring(xmlStr);
            
            doc.getDocumentElement().normalize();
            rootNode=doc.getDocumentElement();
            rootname=rootNode.getNodeName();
            if ~(strcmp(rootname,entityname)==1)
                error('Root element should be entityname');
            end
            
            setOsInterfaces=mcmcInterface.osInterfaces.readStructFromXMLNode(rootNode, myrepos);
        end  

        function setOsInterfaces=readStructFromXMLNode(rootNode, myrepos)        
            %readStructFromXMLNode
            %   Worker function that parses and returns information within a structure that represents a root element.  Function can also be called
            %with a sparse structure, that only contains some (and not all) fields in the root element (so this function must handle missing entries).
            % rootNode-data structure with the xml node read in via xmlread
            % myrepos (optional)-myrepos object with base dir and list of repositories

            stdout=1;
            fprintf(stdout, '\tLoading the osInterfaces from rootNode\n');

            %check version attribute
            tversion=cast(rootNode.getAttribute('version'),'char');
            if (strcmp(mcmcInterface.osInterfaces.getVersion(),tversion)~=1)
                error('OsInterfaces version does not match in *.xml');
            end

            setOsInterfaces.type=cast(rootNode.getAttribute('type'),'char');            

            loadStatus.dirOsInterfaces=false;
            loadStatus.fileOsInterfaces=false;
            for i=1:rootNode.getLength()
                tNode=rootNode.item(i-1);
                if (strcmp(tNode.getNodeName,'dir')==1)
                    tDir=mcmcInterface.storageObjectPath(tNode,myrepos);
                    if (strcmp(tDir.name,'dirOsInterfaces')==1)
                        loadStatus.dirOsInterfaces=true;
                        setOsInterfaces.dirOsInterfaces=tDir.path;
                    elseif (strcmp(tDir.name,'dirTemp')==1)
                        setOsInterfaces.dirTemp=tDir.path;
                    elseif (strcmp(tDir.name,'dirLogs')==1)
                        setOsInterfaces.dirLogs=tDir.path;
                    else
                        error('unexpected xml node name encountered for dir');
                    end
                elseif (strcmp(tNode.getNodeName,'file')==1)
                    tFile=mcmcInterface.storageObjectPath(tNode,myrepos);
                    if (strcmp(tFile.name,'python')==1)
                        setOsInterfaces.python=tFile.path;
                    elseif (strcmp(tFile.name,'javac')==1)
                        setOsInterfaces.javac=tFile.path;
                    elseif (strcmp(tFile.name,'fileOsInterfaces')==1)
                        if (loadStatus.dirOsInterfaces==false)
                            error('need to have dirOsInterfaces in xml');
                        end
                        loadStatus.fileOsInterfaces=true;
                        setOsInterfaces.fileOsInterfaces=tFile.path;
                        tfilename=strcat(setOsInterfaces.dirOsInterfaces,mcmcInterface.osInterfaces.getNativeSeparator(),setOsInterfaces.fileOsInterfaces);
                        setOsInterfacesOverload=mcmcInterface.osInterfaces.readStructFromXMLFile(tfilename, myrepos);
                        setOsInterfaces=mcmcInterface.osInterfaces.updateStruct(setOsInterfaces,setOsInterfacesOverload);
                    elseif (strcmp(tFile.name,'fileKill')==1)
                        setOsInterfaces.fileKill=tFile.path;
                    else
                        error('unexpected xml node name encountered for file');
                    end
                elseif (strcmp(tNode.getNodeName,'pythonUtilsPackage')==1)
                    setOsInterfaces.pythonUtilsPackage=cast(tNode.getTextContent(),'char');
                end
            end 
        end

        function setOsInterfaces=updateStruct(setOsInterfacesOriginal,setOsInterfacesOverload)  
            stdout=1;
            fprintf(stdout, '\tOverloading values for the osInterfaces\n');

            setOsInterfaces=setOsInterfacesOriginal;

            overloadfieldnameslist=fieldnames(setOsInterfacesOverload);
            if(any(contains(overloadfieldnameslist,'type')))
                setOsInterfaces.type=setOsInterfacesOverload.type;
            end
            if(any(contains(overloadfieldnameslist,'dirOsInterfaces')))
                setOsInterfaces.dirOsInterfaces=setOsInterfacesOverload.dirOsInterfaces;
            end
            if(any(contains(overloadfieldnameslist,'fileOsInterfaces')))
                setOsInterfaces.fileOsInterfaces=setOsInterfacesOverload.fileOsInterfaces;
            end
            if(any(contains(overloadfieldnameslist,'dirTemp')))
                setOsInterfaces.dirTemp=setOsInterfacesOverload.dirTemp;
            end
            if(any(contains(overloadfieldnameslist,'dirLogs')))
                setOsInterfaces.dirLogs=setOsInterfacesOverload.dirLogs;
            end
            if(any(contains(overloadfieldnameslist,'fileKill')))
                setOsInterfaces.fileKill=setOsInterfacesOverload.fileKill;
            end
            if(any(contains(overloadfieldnameslist,'python')))
                setOsInterfaces.python=setOsInterfacesOverload.python;
            end
            if(any(contains(overloadfieldnameslist,'pythonUtilsPackage')))
                setOsInterfaces.pythonUtilsPackage=setOsInterfacesOverload.pythonUtilsPackage;
            end
            if(any(contains(overloadfieldnameslist,'javac')))
                setOsInterfaces.javac=setOsInterfacesOverload.javac;
            end
        end

        function retObj = loadAndConstructOsInterface(xmlfile)
            setCfgOsInterfaces=mcmcInterface.osInterfaces.readStructFromXMLFile(xmlfile);		
            retObj=mcmcInterface.osInterfaces.constructor(setCfgOsInterfaces);
        end

        function retObj = constructor(setOsInterfaces)
        %python dependency on psutil `$ sudo apt-get install python-psutil`

            stdout=1;
    		fprintf(stdout, 'Constructing the osInterfaces object\n');

            type=mcmcInterface.osInterfaces.getOSInterfaceType();
            switch type
                case mcmcInterface.t_osInterfaces.MacOS
                    retObj=mcmcInterface.MacOS(setOsInterfaces.dirTemp,setOsInterfaces.dirLogs,setOsInterfaces.fileKill,setOsInterfaces.python,setOsInterfaces.pythonUtilsPackage,setOsInterfaces.javac,setOsInterfaces.dirOsInterfaces,setOsInterfaces.fileOsInterfaces);   
                case mcmcInterface.t_osInterfaces.LinuxOS
                    retObj=mcmcInterface.LinuxOS(setOsInterfaces.dirTemp,setOsInterfaces.dirLogs,setOsInterfaces.fileKill,setOsInterfaces.python,setOsInterfaces.pythonUtilsPackage,setOsInterfaces.javac,setOsInterfaces.dirOsInterfaces,setOsInterfaces.fileOsInterfaces);   
                case mcmcInterface.t_osInterfaces.WindowsOS
                    retObj=mcmcInterface.WindowsOS(setOsInterfaces.dirTemp,setOsInterfaces.dirLogs,setOsInterfaces.fileKill,setOsInterfaces.python,setOsInterfaces.pythonUtilsPackage,setOsInterfaces.javac,setOsInterfaces.dirOsInterfaces,setOsInterfaces.fileOsInterfaces);
                otherwise
                    error('unsupported mcmcInterface.t_osInterfaces');
            end                 
        end

        function createStartupFile() 
        %python3 mcmcinterface/code/genstartup.py --myrepos /sandboxes/johnsra2/study2023a/.mrconfig --outfile code/matlab/startup.m

        end        
        
        function mcmcInterfaceDir=getmcmcInterfacePath()
            OSInterfaceType=mcmcInterface.osInterfaces.getOSInterfaceType();
            switch OSInterfaceType
                case {mcmcInterface.t_osInterfaces.LinuxOS,mcmcInterface.t_osInterfaces.MacOS}
                    [tempDir,namestr]=fileparts(mfilename('fullpath'));
                    tempDir=strcat(tempDir,'/');
                    mcmcInterfaceDir=strrep(tempDir,'/code/matlab/+mcmcInterface/@osInterfaces/','');
                case {mcmcInterface.t_osInterfaces.WindowsOS}
                    [tempDir,namestr]=fileparts(mfilename('fullpath'));
                    tempDir=strcat(tempDir,'\');
                    mcmcInterfaceDir=strrep(tempDir,'\code\matlab\+mcmcInterface\@osInterfaces\','');
                otherwise
                    error('Unsupported mcmcInterface.t_osInterfaces');
            end
        end
        
        function retOSInterfaceType=getOSInterfaceType()
            %use MATLAB's computer variable to determine the OS type
            if strcmp(computer, 'GLNXA64')%use MATLAB to detect OS type
                retOSInterfaceType=mcmcInterface.t_osInterfaces.LinuxOS;                   
            elseif strcmp(computer, 'PCWIN64')                                
                retOSInterfaceType=mcmcInterface.t_osInterfaces.WindowsOS; 
            elseif strcmp(computer, 'MACI64')
                retOSInterfaceType=mcmcInterface.t_osInterfaces.MacOS;
            else
                error('unsupported mcmcInterface.t_osInterfaces');
            end                 
        end            
        
        function retPath=convPathStr(path)
            OSInterfaceType=mcmcInterface.osInterfaces.getOSInterfaceType();
            switch OSInterfaceType
                case {mcmcInterface.t_osInterfaces.LinuxOS,mcmcInterface.t_osInterfaces.MacOS}
                    retPath=strrep(path,'\','/');
                case {mcmcInterface.t_osInterfaces.WindowsOS}
                    retPath=strrep(path,'/','\');
                otherwise
                    error('Unsupported mcmcInterface.t_osInterfaces');
            end
        end
        
        function retString=getNativeSeparator()
            OSInterfaceType=mcmcInterface.osInterfaces.getOSInterfaceType();
            switch OSInterfaceType
                case {mcmcInterface.t_osInterfaces.LinuxOS,mcmcInterface.t_osInterfaces.MacOS}
                    retString='/';
                case {mcmcInterface.t_osInterfaces.WindowsOS}
                    retString='\';
                otherwise
                    error('Unsupported mcmcInterface.t_osInterfaces');
            end
        end        
        
        function retPath=convPathStrUnix(path)
            switch mcmcInterface.osInterfaces.getOSInterfaceType()
                case {mcmcInterface.t_osInterfaces.WindowsOS}
                    retPath=strrep(path,':','');
                    retPath=strrep(retPath,'\','/');
                    retPath=sprintf('/%s',retPath);
                otherwise
                    retPath=path;
            end            
        end            

        function retPath=convPathStrWSLUnix(path)
            switch mcmcInterface.osInterfaces.getOSInterfaceType()
                case {mcmcInterface.t_osInterfaces.WindowsOS}
                    retPath=strrep(path,':','');
                    retPath=strrep(retPath,'\','/');
                    retPath=strcat(lower(retPath(1)),retPath(2:end));%make drive letter lowercase
                    retPath=sprintf('/mnt/%s',retPath);
                otherwise
                    retPath=path;
            end            
        end              
        
        function waitFileExists(fileName)
            stdout=1;
            %fprintf(stdout, '\tdebug: waiting for %s\n',fileName);
            while ~mcmcInterface.osInterfaces.doesFileExist(fileName)  
                pause(0.1)
                drawnow
            end             
            %fprintf(stdout, '\tdebug: located %s\n',fileName);            
        end        
        
        function retStatus=waitFileExists_timeout(fileName,timeout)
            stdout=1;
            timerStart=tic;
            
            retStatus=0;
            %fprintf(stdout, '\tdebug: waiting for %s\n',fileName);
            while ~mcmcInterface.osInterfaces.doesFileExist(fileName)
                pause(0.1)
                drawnow
                elapsedtime=toc(timerStart);
                if elapsedtime>timeout
                    fprintf(stdout, 'Timeout waiting on file: %s\n',fileName);
                    retStatus=-1;
                    break;
                end
            end
            %fprintf(stdout, '\tdebug: located %s\n',fileName);            
        end
        
        function retExists=doesFileExist(fileName)
            retOSInterfaceType=mcmcInterface.osInterfaces.getOSInterfaceType();
            switch retOSInterfaceType
                case {mcmcInterface.t_osInterfaces.WindowsOS}
                    tmpName=mcmcInterface.osInterfaces.convPathStr(fileName);
                otherwise
                    tmpName=mcmcInterface.osInterfaces.convPathStrUnix(fileName);
            end
            file=java.io.File(tmpName);
            if file.exists()%simpler and more reliable existence check for a file (vs MATLAB's exist)
                retExists=true;
            else
                retExists=false;
            end
        end
        
        function clearAllStatic(xmlfile)
            cfgOsInterfaces=mcmcInterface.osInterfaces.readStructFromXMLFile(xmlfile);
            mcmcInterface.osInterfaces.clearAllWorker(cfgOsInterfaces.dirTemp,cfgOsInterfaces.dirLogs);        
        end

        function clearAllWorker(dirTemp, dirLogs)           
            stdout=1;

            fprintf(stdout,'Killing off any open diary file handles\n');
            diary off;

            %Clear temp and logs directories
            fprintf(stdout,'Clearing Temp Directory\n');
            mcmcInterface.osInterfaces.clearDirectory(dirTemp);
            fprintf(stdout,'Clearing Logs Directory\n');
            mcmcInterface.osInterfaces.clearDirectory(dirLogs);            
        end        
        
        function clearDirectory(directory)
            fclose('all');%sometimes MATLAB leaves these open
            stdout=1;
            
            files = dir(fullfile(directory,'*'));
            for fileindex=1:numel(files)
                if strcmp(files(fileindex).name,'.')==0 && strcmp(files(fileindex).name,'..')==0
                    filename=sprintf('%s%s%s',directory,mcmcInterface.osInterfaces.getNativeSeparator(),files(fileindex).name);
                    %fprintf(stdout,'Deleting %s.\n',filename);
                    delete(filename);
                end
            end            
        end         

        function retver=getVersion()%this allows us to read the private constant
            retver=mcmcInterface.osInterfaces.version;
        end        
    end
    
    methods
        function obj = osInterfaces(setType,setDirTemp,setDirLogs,setFileKill,setPython,setPythonUtilsPackage,setJavac,setDirOsInterfaces,setFileOsInterfaces)
            %OSINTERFACES Construct an instance of this class
            if nargin > 0
                obj.type = setType;
                obj.dirTemp = setDirTemp;
                obj.dirLogs = setDirLogs;
                obj.fileKill = setFileKill;
                obj.python = setPython;
                obj.pythonUtilsPackage=setPythonUtilsPackage;
                obj.javac=setJavac;
                obj.dirOsInterfaces=setDirOsInterfaces;
                obj.fileOsInterfaces=setFileOsInterfaces;
            end
        end
        
        function clearAll(obj)
            mcmcInterface.osInterfaces.clearAllWorker(obj.dirTemp,obj.dirLogs);
        end

        function checkForKillFile(obj)
%todo: use os-agnostic-utils
            if mcmcInterface.osInterfaces.doesFileExist(obj.fileKill)%if the kill file exists, quit this MATLAB instance
                stdout=1;
                disp('################################################');
                fprintf(stdout,'Kill file detected, quitting MATLAB in 5 seconds');
                setDesktopStatus('Kill file detected, quitting MATLAB in 5 seconds')
                if obj.bashCmdState==mcmcInterface.t_runState.Running
                    obj.killPIDS(obj.bashCmdPid);
                end
                disp('################################################');
                pause(5);
                quit force;
            end
        end
        
        function deleteCmdMsgFile(obj)
            stdout=1;
            %disp('################################################');
            %fprintf(stdout,'\tdebug: printing callstack\n');
            %dbstack();            
            %disp('################################################');
            %fprintf(stdout,'\tdebug: deleting %s\n',obj.bashCmdMsgFile);
            delete(obj.bashCmdMsgFile)
        end        
        
        function runBashCommand(obj, bashCmdShortcut, bashCmdLogFile, runView)
%TODO: should be os specific?         
            %all the paths in the constructed python script should use forward slashes '/'
            stdout=1;
            obj.bashCmdUuid=java.util.UUID.randomUUID;                                 
            
            shellCmd=mcmcInterface.osInterfaces.convPathStrUnix(bashCmdShortcut);
            obj.bashCmdMsgFile = strcat(obj.dirTemp, mcmcInterface.osInterfaces.getNativeSeparator(), cast(obj.bashCmdUuid.toString(),'char'), '-bashCmdStatus', '.txt');
            obj.bashCmdLogFile=mcmcInterface.osInterfaces.convPathStrUnix(bashCmdLogFile);
            shellCmdRunView=runView.tostring();
            argsstr=sprintf('RUNBASHCMD --msgFile %s --shellRunView %s --shellLog %s --shellCmd %s', ...
                obj.bashCmdMsgFile, shellCmdRunView, obj.bashCmdLogFile, shellCmd);

            commandstr=sprintf('%s -m %s %s', obj.python, obj.pythonUtilsPackage, argsstr);
            %fprintf(stdout,'\tdebug: runBashCommand is calling:  %s\n',commandstr);
            [status,cmdout]=system(commandstr);
            %fprintf(stdout,'\tdebug: runBashCommand system() returned status=%d, cmdout=%s\n',status,cmdout);
            
            if status==0 %ok
                status=mcmcInterface.osInterfaces.waitFileExists_timeout(obj.bashCmdMsgFile, obj.cTIMEOUT);

                if (not(status==0))
                    error('Error with runBashCommand()'); 
                end

                obj.bashCmdName=shellCmd;
                %fprintf(stdout,'PROCESSNAME=%s\n',obj.bashCmdName);                            
                                
                ready=false;                
                while ready==false
                    pause(0.1);
                    drawnow;
                    querystr=fquery(obj.bashCmdMsgFile,'##PID##');
                    if ~strcmp(querystr,'NOTFOUND') && ~isempty(querystr)
                        obj.bashCmdPid=sscanf(querystr,'%d');
                        %fprintf(stdout,'PID=%d\n',obj.bashCmdPid);                        
                        querystr=fquery(obj.bashCmdMsgFile,'##STATE##');%STATE is written last
                        if ~strcmp(querystr,'NOTFOUND') && ~isempty(querystr)                           
                            querystr=fquery(obj.bashCmdMsgFile,'##PROCESSCMDLINE##');
                            if ~strcmp(querystr,'NOTFOUND') && ~isempty(querystr)
                                obj.bashCmdLine=querystr;                            
                                %fprintf(stdout,'PROCESSCMDLINE=%s\n',obj.bashCmdLine);
                                ready=true;
                            end
                        end
                    end                    
                end      
            else
                error('Error with runBashCommand()');                
            end
        end

        function spawnBashCommand(obj, bashCmdShortcut, bashCmdLogFile, runView)            
            stdout=1;
            %all the paths in the constructed python script should use forward slashes '/'
            obj.bashCmdUuid=java.util.UUID.randomUUID;

            %remove all the prefix path and '/' or '\' characters and use the binary or script name
            if max(strfind(bashCmdShortcut,mcmcInterface.osInterfaces.getNativeSeparator()))>1
                startchar=max(strfind(bashCmdShortcut,mcmcInterface.osInterfaces.getNativeSeparator()));
            else
                startchar=1;
            end
            shellTitle=bashCmdShortcut(startchar+1:end);

            switch obj.type
                case {mcmcInterface.t_osInterfaces.WindowsOS}                   
                    obj.bashCmdLogFile=mcmcInterface.osInterfaces.convPathStrWSLUnix(bashCmdLogFile);
                    shellCmd=mcmcInterface.osInterfaces.convPathStrWSLUnix(bashCmdShortcut);
                    obj.bashCmdMsgFile = strcat(obj.dirTemp, '\', cast(obj.bashCmdUuid.toString(),'char'), '-bashCmdStatus', '.txt');                    
                case {mcmcInterface.t_osInterfaces.MacOS,mcmcInterface.t_osInterfaces.LinuxOS}
                    obj.bashCmdLogFile=bashCmdLogFile;
                    shellCmd=bashCmdShortcut;
                    obj.bashCmdMsgFile = strcat(obj.dirTemp, '/', cast(obj.bashCmdUuid.toString(),'char'), '-bashCmdStatus', '.txt');                    
            end
            shellCmdRunView=runView.tostring();            
            
            switch obj.type
                case {mcmcInterface.t_osInterfaces.MacOS,mcmcInterface.t_osInterfaces.LinuxOS}
                    argsstr=sprintf('SPAWNBASHCMD --debug --msgFile %s --shellRunView %s --shellLog %s --shellTitle %s --shellCmd %s', ...
                        obj.bashCmdMsgFile, shellCmdRunView, obj.bashCmdLogFile, shellTitle, shellCmd);
                    commandstr=sprintf('unset LD_LIBRARY_PATH && unset PYTHONPATH && %s -m %s %s', obj.python, obj.pythonUtilsPackage, argsstr);%For Ubuntu, we have an issue with building CmdStan and the LD_LIBRARY_PATH env variable that MATLAB sets; just unset it for a simple fix

                case {mcmcInterface.t_osInterfaces.WindowsOS}                   
                    argsstr=sprintf('SPAWNBASHCMD --debug --msgFile %s --shellRunView %s --shellLog %s --shellTitle "%s" --shellCmd %s', ...
                        obj.bashCmdMsgFile, shellCmdRunView, obj.bashCmdLogFile, shellTitle, shellCmd);
                    %Windows only works with start, need to determine why that is and we can't just use python.exe
                    commandstr=sprintf('%s -m %s %s', obj.python, obj.pythonUtilsPackage, argsstr);
            end
            fprintf(stdout,'\tdebug: spawnBashCommand is calling:  %s\n',commandstr);
            [status,cmdout]=system(commandstr);
            fprintf(stdout,'\tdebug: spawnBashCommand system() returned status=%d, cmdout=%s\n',status,cmdout);
            
            if status==0 %ok
                status=mcmcInterface.osInterfaces.waitFileExists_timeout(obj.bashCmdMsgFile, obj.cTIMEOUT);

                if (not(status==0))
                    error('Error with spawnBashCommand()'); 
                end

                obj.bashCmdName=shellCmd;
                %fprintf(stdout,'PROCESSNAME=%s\n',obj.bashCmdName);                            
                
                ready=false;                
                while ready==false
                    pause(0.1);
                    drawnow;
                    querystr=fquery(obj.bashCmdMsgFile,'##PID##');
                    if ~strcmp(querystr,'NOTFOUND') && ~isempty(querystr)
                        obj.bashCmdPid=sscanf(querystr,'%d');
                        %fprintf(stdout,'PID=%d\n',obj.bashCmdPid);                        
                        querystr=fquery(obj.bashCmdMsgFile,'##STATE##');%STATE is written last
                        if ~strcmp(querystr,'NOTFOUND') && ~isempty(querystr)                           
                            querystr=fquery(obj.bashCmdMsgFile,'##PROCESSCMDLINE##');
                            if ~strcmp(querystr,'NOTFOUND') && ~isempty(querystr)
                                obj.bashCmdLine=querystr;                            
                                %fprintf(stdout,'PROCESSCMDLINE=%s\n',obj.bashCmdLine);
                                ready=true;
                            end
                        end
                    end                    
                end      
            else
                error('Error with runBashCommand()');                
            end            
        end        
        
        function [commandstr]=updateBashCmdStatus(obj, setcommandstr) %e.g., only care about status for MCMC sampling requests           
            stdout=1;
            if (isempty(setcommandstr)) %not regenerating commandstr because of a potential memory leak with java when this is run thousands of times
                argsstr=sprintf('QUERYBASHCMDSTATUS --debug --shellRunView mcmcBayes.t_runView.Background --msgFile %s --processPID=%d', obj.bashCmdMsgFile, obj.bashCmdPid);
                switch obj.type
                    case {mcmcInterface.t_osInterfaces.MacOS,mcmcInterface.t_osInterfaces.LinuxOS}
                        commandstr=sprintf('unset LD_LIBRARY_PATH && unset PYTHONPATH && %s -m %s %s', obj.python, obj.pythonUtilsPackage, argsstr);%For Ubuntu, we have an issue with building CmdStan and the LD_LIBRARY_PATH env variable that MATLAB sets; just unset it for a simple fix
                    case {mcmcInterface.t_osInterfaces.WindowsOS}   
                        %Windows only works with start, need to determine why that is and we can't just use python.exe
                        commandstr=sprintf('%s -m %s %s', obj.python, obj.pythonUtilsPackage, argsstr);
                end
            else%otherwise, we use the one passed in
                commandstr=setcommandstr;
            end
            %fprintf(stdout,'\tdebug: updateBashCmdStatus is calling:  %s\n',commandstr);
            [status,cmdout] = system(commandstr);%updateBashCmdStatus will return cmdout if you specify --debug (except on Windows when called via pythonw)
            %fprintf(stdout,'\tdebug: updateBashCmdStatus system() returned status=%d, cmdout=%s\n',status,cmdout);
            
            if status==0 %ok
                mcmcInterface.osInterfaces.waitFileExists(obj.bashCmdMsgFile)    

                statusstr=fquery(obj.bashCmdMsgFile,'##STATE##');
                if ~strcmp(statusstr,'NOTFOUND')
                    statusstr=sscanf(statusstr,'%s');
                    obj.bashCmdState=mcmcInterface.t_runState.fromstring(strcat('mcmcInterface.t_runState.',statusstr));
                    if obj.bashCmdState==mcmcInterface.t_runState.Finished || obj.bashCmdState==mcmcInterface.t_runState.Error
                        exitcodestr=fquery(obj.bashCmdMsgFile,'##EXITCODE##');%read the exitcode from the UUIDfilestr
                        if ~strcmp(exitcodestr,'NOTFOUND')
                            obj.bashCmdState=mcmcInterface.t_runState.Finished;
                            obj.bashCmdExitCode=sscanf(exitcodestr,'%d');
                            fprintf(stdout,'\tdebug: bashcmdexitcode=%d\n',obj.bashCmdExitCode);
                            obj.deleteCmdMsgFile();
                        end
                    end
                end
            else
                error('Error running updateSamplerStatus()');
            end
        end   
    end
    
    methods %getter/setter functions
        function set.dirTemp(obj,setDirTemp) 
            if mcmcInterface.osInterfaces.doesFileExist(setDirTemp) %is a folder
                obj.dirTemp=setDirTemp;
            else
                mkdir(setDirTemp);%try to create it if it does not exist
                if mcmcInterface.osInterfaces.doesFileExist(setDirTemp) %is a folder
                    obj.dirTemp=setDirTemp;
                else
                    error('error creating dirTemp folder');
                end
            end
        end
        
        function set.dirLogs(obj,setDirLogs) 
            if mcmcInterface.osInterfaces.doesFileExist(setDirLogs) %is a folder
                obj.dirLogs=setDirLogs;
            else
                mkdir(setDirLogs);%try to create it if it does not exist
                if mcmcInterface.osInterfaces.doesFileExist(setDirLogs) %is a folder
                    obj.dirLogs=setDirLogs;
                else
                    error('error creating dirLogs folder');
                end
            end
        end

        function set.type(obj, setType)
            if ~(isa(setType,'mcmcInterface.t_osInterfaces'))
                error('setType is not mcmcInterface.t_osInterfaces object');
            end
            obj.type=setType;
        end       
    end
end

