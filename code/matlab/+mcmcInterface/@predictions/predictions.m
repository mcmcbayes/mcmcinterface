classdef predictions < mcmcInterface.data
    %PREDICTIONS
    %   This class, mcmcInterface.predictions, is a data structure used to contain a predictions set for the simulation.  
    
    properties (Constant, Access = private) %define as private so that subclasses can have their own instances with same name
        version='1.0';      % API version for this class
        hingeThreshold=0.05;
    end
    
    methods (Static)
        function setPredictions=readStructFromXMLNode(rootNode, myrepos)
            %readStructFromXMLNode
            %   Worker function that parses and returns information within a structure that represents a root element.  Function can also be called
            %with a sparse structure, that only contains some (and not all) fields in the root element (so this function must handle missing entries).
            % rootNode-data structure with the xml node read in via xmlread
            % myrepos (optional)-myrepos object with base dir and list of repositories
            stdout=1;
            fprintf(stdout, '\tLoading the predictions from rootNode\n');
            
            %check version attribute
            tversion=cast(rootNode.getAttribute('version'),'char');
            if (strcmp(mcmcInterface.predictions.getVersion(),tversion)~=1)
                error('predictions version does not match in *.xml');
            end

            for i=1:rootNode.getLength()
                tNode=rootNode.item(i-1);
                if (strcmp(tNode.getNodeName,'data')==1)
                    setPredictions=mcmcInterface.data.readStructFromXMLNode(tNode);
                end
            end
        end
    
        function retPredictions=constructor(setPredictions)
            stdout=1;
    		fprintf(stdout, 'Constructing the predictions object\n');
            for i=1:size(setPredictions.elements,1)
                setElements(i,1)=mcmcInterface.element.constructor(setPredictions.elements(i));
            end
            retPredictions=mcmcInterface.predictions(setPredictions.uuid,setPredictions.description,setPredictions.author,setPredictions.reference,setElements,setPredictions.dirData,setPredictions.fileData);
        end          

        function retver=getVersion()%this allows us to read the private constant
            retver=mcmcInterface.predictions.version;
        end      

        function [retStatistics]=computeStatistics(statisticsToCapture, edges, dimensions, datatype, sampledata)
            %This function bins the contents of sampledata and computes statistics for the mean, lower and upper hinges.  A histogram of the sampledata is
            %created using the provided bin thresholds, and a cdf is approximated by performing a cumulative sum on the histogram and dividing it by the number
            %of datapoints.  The mean is simply the average of the sampledata.  The hinges are estimated by identifying the lower and upper thresholds in the
            %approximated cdf where samples fall under low X% or above high X% (e.g., 5% lower hinge would define 0-5% set of smallest samples and a 
            %5% upper hinge would define 95-100% set of largest values).

            %statisticsToCapture is a vector of mcmcInterface.t_predictionStatistic
            %bins is a vector with the edges used for the histogram bins
            %depdatavarid is the index for the dependent variable element in obj.elements          

            %for sampledata:
            %N=number of datapoints
            %R=number of rows
            %C=number of columns
            if dimensions==mcmcInterface.t_valuesDimensions.scalar
                N=size(sampledata,1);
                R=1;
                C=1;
            elseif dimensions==mcmcInterface.t_valuesDimensions.vector
                R=1;
                N=size(sampledata,1);
                C=size(sampledata,2);
            elseif dimensions==mcmcInterface.t_valuesDimensions.matrix2D
                R=size(sampledata,1);
                C=size(sampledata,2);
                N=size(sampledata,3);
            end                  
         
            if N>1%only compute the histogram when predictionsNSamples>1
                if dimensions==mcmcInterface.t_valuesDimensions.scalar
                    histout=zeros(1,size(edges,2)-1);%need to create a t=zero histogram
                    [thistN,edges]=histcounts(sampledata(:,1), edges);%approximate pdf
                    histout(1,:)=cumsum((1/cast(N,'double'))*thistN);%approximate cdf    
                    for i=1:size(statisticsToCapture,1)%use the vector obj.dependentVariablesHatTypes to determine which rows from dependentVariablesHat to show
                        Yhattype=statisticsToCapture(i);
                        switch Yhattype
                            case mcmcInterface.t_predictionStatistic.hingelo %threshold for 5%
                                tdata=cast(edges(find(histout(1,:)>mcmcInterface.predictions.hingeThreshold,1,'first')),datatype);
                                tHingelo=tdata;%need to update all the array values for this struct in one operation
                            case mcmcInterface.t_predictionStatistic.mean %threshold for 50%
                                tdata=cast(edges(find(histout(1,:)>.50,1,'first')),datatype);
                                tMean=tdata;%need to update all the array values for this struct in one operation
                            case mcmcInterface.t_predictionStatistic.hingehi %threshold for 95%
                                tdata=cast(edges(find(histout(1,:)>(1-mcmcInterface.predictions.hingeThreshold),1,'first')),datatype);
                                tHingehi=tdata;%need to update all the array values for this struct in one operation
                        end
                    end
                elseif dimensions==mcmcInterface.t_valuesDimensions.vector
                    histout=zeros(size(edges,2)-1,C);%need to create a t=zero histogram
                    for datapoint=1:C
                        if C==1
                            [thistN,edges]=histcounts(sampledata(:,1), edges);%approximate pdf
                        else
                            [thistN,edges]=histcounts(sampledata(:,datapoint), edges);%approximate pdf
                        end
                        histout(:,datapoint)=cumsum((1/cast(N,'double'))*thistN);%approximate cdf
                    end
    
                    for i=1:size(statisticsToCapture,1)%use the vector obj.dependentVariablesHatTypes to determine which rows from dependentVariablesHat to show
                        Yhattype=statisticsToCapture(i);
                        for datapoint=1:C
                            switch Yhattype
                                case mcmcInterface.t_predictionStatistic.hingelo %threshold for 5%
                                    tdata(1,datapoint)=cast(edges(find(histout(:,datapoint)>mcmcInterface.predictions.hingeThreshold,1,'first')),datatype);
                                case mcmcInterface.t_predictionStatistic.mean %threshold for 50%
                                    tdata(1,datapoint)=cast(edges(find(histout(:,datapoint)>.50,1,'first')),datatype);
                                case mcmcInterface.t_predictionStatistic.hingehi %threshold for 95%
                                    tdata(1,datapoint)=cast(edges(find(histout(:,datapoint)>(1-mcmcInterface.predictions.hingeThreshold),1,'first')),datatype);
                            end
                        end
                        switch Yhattype
                            case mcmcInterface.t_predictionStatistic.hingelo %threshold for 5%
                                tHingelo=tdata;%need to update all the array values for this struct in one operation
                            case mcmcInterface.t_predictionStatistic.mean %threshold for 50%
                                tMean=tdata;%need to update all the array values for this struct in one operation
                            case mcmcInterface.t_predictionStatistic.hingehi %threshold for 95%
                                tHingehi=tdata;%need to update all the array values for this struct in one operation
                        end
                    end
                else
                    error('matrix2D is unsupported');
                end
            else%only generated 1 sample (is this case still needed after all the refactoring?)
                for i=1:size(statisticsToCapture,1)%use the vector obj.dependentVariablesHatTypes to determine which rows from dependentVariablesHat to show
                    Yhattype=statisticsToCapture(i);
                    switch Yhattype
                        case mcmcInterface.t_predictionStatistic.hingelo %threshold for 5%
                            tHingelo=sampledata;         
                        case mcmcInterface.t_predictionStatistic.mean %threshold for 50%
                            tMean=sampledata;         
                        case mcmcInterface.t_predictionStatistic.hingehi %threshold for 95%
                            tHingehi=sampledata;         
                    end
                end                                      
            end

            if isempty(tMean)
                tMean=[];
            end
            tMedian=[];
            tSd=[];
            tMin=[];
            tMax=[];
            tHdrlo=[];
            tHdrhi=[];
            tMce=[];
            if isempty(tHingelo)
                tHingelo=[];
            end
            if isempty(tHingehi)
                tHingehi=[];
            end            

            retStatistics=mcmcInterface.statistics(tMean,tMedian,tSd,tMin,tMax,tHdrlo,tHdrhi,tMce);
            retStatistics.hingelo=tHingelo;
            retStatistics.hingehi=tHingehi;
        end        
    end

    methods
        function obj = predictions(setUuid,setDescription,setAuthor,setReference,setElements,setDirData,setFileData)
            if nargin > 0
                superargs{1}=setUuid;
                superargs{2}=setDescription;
                superargs{3}=setAuthor;
                superargs{4}=setReference;
                superargs{5}=setElements;
                superargs{6}=setDirData;
                superargs{7}=setFileData;
            elseif nargin == 0
                superargs={};
            end
            obj=obj@mcmcInterface.data(superargs{:});
        end
    end
end

