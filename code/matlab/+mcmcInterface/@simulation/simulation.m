classdef simulation
    %SIMULATION
    %   This class, mcmcInterface.simulation, contains functions needed to perform MCMC or HMC simulations and a 
    % data structure with information about a specific simulation (i.e., single sampling run).  It is the main 
    % object used to control a sampling session.  It could be used stand-alone for demonstration purposes (not 
    % recommended), but it is intended to be used within mcmcBayes where it would be located in the simulations 
    % array within mcmcBayes.sequence objects.  

    properties (Constant, Access = private) %define as private so that subclasses can have their own instances with same name
        version='1.0';              % API version for this class
    end

    properties
        uuid                        % unique identifier for the simulation

        dirSimulation               % directory that contains *_simulation.xml file to load
        fileSimulation              % filename of the *_simulation.xml file to load

        fileNamePrefix              % naming prefix for all output files created by the running simulation
        fileLog                     % log file created by the running simulation
        
        sampleResults               % when false, mcmcInterface.simulation.runSimulationWorker loads previously sampled values from file archives
        archiveSamples              % when true, samples will be archived in *.xml
        enableDiagnosticPlots       % when true, figures will be generated from diagnostic plots of the samples
        enableDataPlots             % when true, original data will be plotted
        enablePredictionPlots       % when true, predictions will be plotted
        savePlots                   % when true along with enableDiagnosticPlots, figures will be saved
        runView                     % when mcmcInterface.t_runView.Foreground, plots will be shown to user (MATLAB also steals focus), otherwise they will remain hidden

        hOsInterfaces               % handle to the osInterfaces object for the simulation
        hSamplerInterfaces          % handle to the samplerInterfaces object for the simulation
        data                        % array of data objects for the simulation
        variables                   % array of variables object for the simulation
        samples                     % array of samples object for the simulation

        hModel                      % handle to the model object needed for predictions and transforms on variables, data, and generated elements
        generatePredictions         % when true, predictions will be generated using sample results with the model
        predictionStatisticsToCapture   % array of mcmcInterface.t_predictionStatistic that specify the types of statistics to capture (e.g., mean, hingelo, hingehi, normalized histogram)
        predictions                 % array of predictions object for the simulation (implements a data object with generated predictions for dependent variables)
    end

    methods (Static)
        function setSimulation=readStructFromXMLFile(xmlfile, myrepos)        
            %readStructFromXMLFile 
            %   Load a simulation object from *.xml.
            % xmlfile-simulation xmlfile name (either absolute or relative path inside repos's config directory)
            % myrepos (optional)-myrepos object with base dir and list of repositories
            stdout=1;
            fprintf(stdout, 'Loading %s for the simulation\n',xmlfile);
            
            doc = xmlread(xmlfile);
            %fprintf(1,'Parsed %s\n',xmlfile);
            %javaaddpath(mcmcBayes.osInterfaces.getPath(mcmcBayes.t_paths.javaDir));
            %call our java hacks method to insert the DOCTYPE element
            entityname='simulation';
            %     xmlStr=XmlHacks.insert(doc, entityname, dtdfile);%call Java function  (XmlHacks must be on javaclasspath)
            %     %now parse the hacked xml
            %     doc = XmlHacks.xmlreadstring(xmlStr);

            doc.getDocumentElement().normalize();
            rootNode=doc.getDocumentElement();
            rootname=rootNode.getNodeName();
            if ~(strcmp(rootname,entityname)==1)
                error('Root element should be entityname');
            end

            setSimulation=mcmcInterface.simulation.readStructFromXMLNode(rootNode, myrepos);
        end    

        function setSimulation=readStructFromXMLNode(rootNode, myrepos)  
            %readStructFromXMLNode
            %   Worker function that parses and returns information within a structure that represents a root element.  Function can also be called
            %with a sparse structure, that only contains some (and not all) fields in the root element (so this function must handle missing entries).
            % rootNode-data structure with the xml node read in via xmlread
            % myrepos (optional)-myrepos object with base dir and list of repositories
            
            stdout=1;
            fprintf(stdout, '\tLoading the simulation from rootNode\n');

            %check version attribute
            tversion=cast(rootNode.getAttribute('version'),'char');
            if (strcmp(mcmcInterface.simulation.getVersion(),tversion)~=1)
                error('Simulation version does not match in *.xml');
            end

            %get uuid attribute (need to rewrite it in the fileSimulation case below) 
            temp=cast(rootNode.getAttribute('uuid'),'char');
            if ~isempty(temp)
                setSimulation.uuid=cast(rootNode.getAttribute('uuid'),'char');
            end

            loadStatus.dirSimulation=false;
            loadStatus.fileSimulation=false;
            for i=1:rootNode.getLength()
                tNode=rootNode.item(i-1);
                if (strcmp(tNode.getNodeName,'dir')==1)
                    tDir=mcmcInterface.storageObjectPath(tNode,myrepos);
                    if (strcmp(tDir.name,'dirSimulation')==1)
                        loadStatus.dirSimulation=true;
                        setSimulation.dirSimulation=tDir.path;
                    else
                        error('unexpected xml node name encountered for dir');
                    end
                elseif (strcmp(tNode.getNodeName,'file')==1)
                    tFile=mcmcInterface.storageObjectPath(tNode,myrepos);
                    if (strcmp(tFile.name,'fileSimulation')==1)
                        if (loadStatus.dirSimulation==false)
                            error('need to have dirSimulation in xml');
                        end
                        loadStatus.fileSimulation=true;
                        setSimulation.fileSimulation=tFile.path;
                        tfilename=strcat(setSimulation.dirSimulation,mcmcInterface.osInterfaces.getNativeSeparator(),setSimulation.fileSimulation);
                        setSimulationOverload=mcmcInterface.simulation.readStructFromXMLFile(tfilename, myrepos);
                        setSimulation=mcmcInterface.simulation.updateStruct(setSimulation,setSimulationOverload);
                        %rewrite the uuid attribute for this case
                        %setSimulation.uuid=cast(rootNode.getAttribute('uuid'),'char'); 
                    else
                        error('unexpected xml node name encountered for file');
                    end
                elseif (strcmp(tNode.getNodeName,'fileNamePrefix')==1)
                    setSimulation.fileNamePrefix=cast(tNode.getTextContent(),'char');
                elseif (strcmp(tNode.getNodeName,'fileLog')==1)
                    setSimulation.fileLog=cast(tNode.getTextContent(),'char');
                elseif (strcmp(tNode.getNodeName,'sampleResults')==1)
                    setSimulation.sampleResults=str2num(cast(tNode.getTextContent(),'char'));
                elseif (strcmp(tNode.getNodeName,'archiveSamples')==1)
                    setSimulation.archiveSamples=str2num(cast(tNode.getTextContent(),'char'));
                elseif (strcmp(tNode.getNodeName,'enableDiagnosticPlots')==1)
                    setSimulation.enableDiagnosticPlots=str2num(cast(tNode.getTextContent(),'char'));
                elseif (strcmp(tNode.getNodeName,'enableDataPlots')==1)
                    setSimulation.enableDataPlots=str2num(cast(tNode.getTextContent(),'char'));
                elseif (strcmp(tNode.getNodeName,'enablePredictionPlots')==1)
                    setSimulation.enablePredictionPlots=str2num(cast(tNode.getTextContent(),'char'));
                elseif (strcmp(tNode.getNodeName,'savePlots')==1)
                    setSimulation.savePlots=str2num(cast(tNode.getTextContent(),'char'));
                elseif (strcmp(tNode.getNodeName,'runView')==1)
                    setSimulation.runView=mcmcInterface.t_runView.str2enum(strcat('mcmcInterface.t_runView.',cast(tNode.getTextContent(),'char')));
                elseif (strcmp(tNode.getNodeName,'osInterfaces')==1)
                    simulationfieldnameslist=fieldnames(setSimulation);
                    if(any(contains(simulationfieldnameslist,'osInterfaces')))
                        setOsInterfacesOverload=mcmcInterface.osInterfaces.readStructFromXMLNode(tNode, myrepos);
                        setSimulation.osInterfaces=mcmcInterface.osInterfaces.updateStruct(setSimulation.osInterfaces,setOsInterfacesOverload);
                    else    
                        setSimulation.osInterfaces=mcmcInterface.osInterfaces.readStructFromXMLNode(tNode, myrepos);
                    end
                elseif (strcmp(tNode.getNodeName,'samplerInterfaces')==1)
                    simulationfieldnameslist=fieldnames(setSimulation);
                    if(any(contains(simulationfieldnameslist,'samplerInterfaces')))
                        setSamplerInterfacesOverload=mcmcInterface.samplerInterfaces.readStructFromXMLNode(tNode, myrepos);
                        setSimulation.samplerInterfaces=mcmcInterface.samplerInterfaces.updateStruct(setSimulation.samplerInterfaces,setSamplerInterfacesOverload);
                    else    
                        setSimulation.samplerInterfaces=mcmcInterface.samplerInterfaces.readStructFromXMLNode(tNode, myrepos);
                    end                    
                elseif (strcmp(tNode.getNodeName,'data')==1)
                    simulationfieldnameslist=fieldnames(setSimulation);
                    if(any(contains(simulationfieldnameslist,'data')))
                        setDataOverload=mcmcInterface.data.readStructFromXMLNode(tNode, myrepos);
                        setSimulation.data=mcmcInterface.data.updateStruct(setSimulation.data,setDataOverload);
                    else    
                        setSimulation.data=mcmcInterface.data.readStructFromXMLNode(tNode, myrepos);
                    end                      
                elseif (strcmp(tNode.getNodeName,'variables')==1)
                    simulationfieldnameslist=fieldnames(setSimulation);
                    if(any(contains(simulationfieldnameslist,'variables')))
                        setVariablesOverload=mcmcInterface.variables.readStructFromXMLNode(tNode, myrepos);
                        setSimulation.variables=mcmcInterface.variables.updateStruct(setSimulation.variables,setVariablesOverload);
                    else    
                        setSimulation.variables=mcmcInterface.variables.readStructFromXMLNode(tNode, myrepos);
                    end                       
                elseif (strcmp(tNode.getNodeName,'samples')==1)
                    simulationfieldnameslist=fieldnames(setSimulation);
                    if(any(contains(simulationfieldnameslist,'samples')))
                        setSamplesOverload=mcmcInterface.samples.readStructFromXMLNode(tNode, myrepos);
                        setSimulation.samples=mcmcInterface.samples.updateStruct(setSimulation.samples,setSamplesOverload);
                    else    
                        setSimulation.samples=mcmcInterface.samples.readStructFromXMLNode(tNode, myrepos);
                    end
                elseif (strcmp(tNode.getNodeName,'generatePredictions')==1)
                    setSimulation.generatePredictions=str2num(cast(tNode.getTextContent(),'char'));
                elseif (strcmp(tNode.getNodeName,'predictionStatisticsToCapture')==1)
                    %comma separated list of strings that are t_predictionStatistic values without 't_predictionStatistic.' prepended
                    strlist=cast(tNode.getTextContent(),'char');
                    strvalues=split(strlist,',');
                    for j=1:size(strvalues,1)
                        if ~isempty(strvalues{j})
                            setSimulation.predictionStatisticsToCapture(j,1)=mcmcInterface.t_predictionStatistic.fromstring(strcat('mcmcInterface.t_predictionStatistic.',strvalues{j}));
                        else
                            setSimulation.predictionStatisticsToCapture=mcmcInterface.t_predictionStatistic.empty;
                        end
                    end
                elseif (strcmp(tNode.getNodeName,'predictions')==1)
                    simulationfieldnameslist=fieldnames(setSimulation);
                    if(any(contains(simulationfieldnameslist,'predictions')))
                        setPredictionsOverload=mcmcInterface.predictions.readStructFromXMLNode(tNode, myrepos);
                        setSimulation.predictions=mcmcInterface.predictions.updateStruct(setSimulation.predictions,setPredictionsOverload);
                    else    
                        setSimulation.predictions=mcmcInterface.predictions.readStructFromXMLNode(tNode, myrepos);
                    end
                end
            end

            fieldnameslist=fieldnames(setSimulation);
            if ~(any(contains(fieldnameslist,'sampleResults')))
                setSimulation.sampleResults=true;%default
            end
            if ~(any(contains(fieldnameslist,'archiveSamples')))
                setSimulation.archiveSamples=false;%default
            end
            if ~(any(contains(fieldnameslist,'enableDiagnosticPlots')))
                setSimulation.enableDiagnosticPlots=true;%default
            end
            if ~(any(contains(fieldnameslist,'savePlots')))
                setSimulation.savePlots=true;%default
            end
            if ~(any(contains(fieldnameslist,'enableDiagnosticPlots')))
                setSimulation.enableDiagnosticPlots=true;%default
            end
            if ~(any(contains(fieldnameslist,'enableDataPlots')))
                setSimulation.enableDataPlots=false;%default
            end
            if ~(any(contains(fieldnameslist,'enablePredictionPlots')))
                setSimulation.enablePredictionPlots=false;%default
            end
            if ~(any(contains(fieldnameslist,'runView')))
                setSimulation.runView='Foreground';%default
            end
            if ~(any(contains(fieldnameslist,'generatePredictions')))
                setSimulation.generatePredictions=false;%default
            end
        end

        function setSimulation=updateStruct(setSimulationOriginal,setSimulationOverload)  
            %updateStruct
            %   Overload values from original configuration with non-null ones in overload configuration

            stdout=1;
            fprintf(stdout, '\tOverloading fields in simulation\n');

            setSimulation=setSimulationOriginal;

            simulationfieldnameslist=fieldnames(setSimulationOriginal);
            overloadfieldnameslist=fieldnames(setSimulationOverload);
            if(any(contains(overloadfieldnameslist,'uuid')))
                setSimulation.uuid=setSimulationOverload.uuid;
            end
            if(any(contains(overloadfieldnameslist,'fileNamePrefix')))
                setSimulation.fileNamePrefix=setSimulationOverload.fileNamePrefix;
            end
            if(any(contains(overloadfieldnameslist,'fileLog')))
                setSimulation.fileLog=setSimulationOverload.fileLog;
            end
            if(any(contains(overloadfieldnameslist,'sampleResults')))
                setSimulation.sampleResults=setSimulationOverload.sampleResults;
            end    
            if(any(contains(overloadfieldnameslist,'archiveSamples')))
                setSimulation.archiveSamples=setSimulationOverload.archiveSamples;
            end    
            if(any(contains(overloadfieldnameslist,'enableDiagnosticPlots')))
                setSimulation.enableDiagnosticPlots=setSimulationOverload.enableDiagnosticPlots;
            end              
            if(any(contains(overloadfieldnameslist,'enableDataPlots')))
                setSimulation.enableDataPlots=setSimulationOverload.enableDataPlots;
            end              
            if(any(contains(overloadfieldnameslist,'enablePredictionPlots')))
                setSimulation.enablePredictionPlots=setSimulationOverload.enablePredictionPlots;
            end              
            if(any(contains(overloadfieldnameslist,'savePlots')))
                setSimulation.savePlots=setSimulationOverload.savePlots;
            end              
            if(any(contains(overloadfieldnameslist,'runView')))
                setSimulation.runView=setSimulationOverload.runView;
            end              
            if(any(contains(overloadfieldnameslist,'osInterfaces')))
                if (any(contains(simulationfieldnameslist,'osInterfaces')))                   
                    setSimulation.osInterfaces=mcmcInterface.osInterfaces.updateStruct(setSimulation.osInterfaces,setSimulationOverload.OsInterfaces);
                else
                    setSimulation.osInterfaces=setSimulationOverload.osInterfaces;                    
                end                
            end                 
            if(any(contains(overloadfieldnameslist,'samplerInterfaces')))
                if (any(contains(simulationfieldnameslist,'samplerInterfaces')))                   
                    setSimulation.samplerInterfaces=mcmcInterface.samplerInterfaces.updateStruct(setSimulation.samplerInterfaces,setSimulationOverload.samplerInterfaces);
                else
                    setSimulation.samplerInterfaces=setSimulationOverload.samplerInterfaces;   
                end
            end 
            if(any(contains(overloadfieldnameslist,'data')))
                if (any(contains(simulationfieldnameslist,'data')))                   
                    setSimulation.data=mcmcInterface.data.updateStruct(setSimulation.data,setSimulationOverload.data);
                else
                    setSimulation.data=setSimulationOverload.data;   
                end
            end 
            if(any(contains(overloadfieldnameslist,'variables')))
                if (any(contains(simulationfieldnameslist,'variables')))                   
                    setSimulation.variables=mcmcInterface.variables.updateStruct(setSimulation.variables,setSimulationOverload.variables);
                else
                    setSimulation.variables=setSimulationOverload.variables;   
                end
            end 
            if(any(contains(overloadfieldnameslist,'samples')))
                if (any(contains(simulationfieldnameslist,'samples')))                   
                    setSimulation.samples=mcmcInterface.samples.updateStruct(setSimulation.samples,setSimulationOverload.samples);
                else
                    setSimulation.samples=setSimulationOverload.samples;   
                end
            end
            if(any(contains(overloadfieldnameslist,'generatePredictions')))
                setSimulation.generatePredictions=setSimulationOverload.generatePredictions;
            end
            if(any(contains(overloadfieldnameslist,'predictions')))
                if (any(contains(simulationfieldnameslist,'predictions')))                   
                    setSimulation.predictions=mcmcInterface.data.updateStruct(setSimulation.predictions,setSimulationOverload.predictions);
                else
                    setSimulation.predictions=setSimulationOverload.predictions;   
                end
            end 
            if(any(contains(overloadfieldnameslist,'predictionStatisticsToCapture')))
                if (any(contains(simulationfieldnameslist,'predictionStatisticsToCapture')))                   
                    setSimulation.predictionStatisticsToCapture=mcmcInterface.data.updateStruct(setSimulation.predictionStatisticsToCapture,setSimulationOverload.predictionStatisticsToCapture);
                else
                    setSimulation.predictionStatisticsToCapture=setSimulationOverload.predictionStatisticsToCapture;   
                end
            end             
        end
        
        function retObj=loadAndConstructSimulation(xmlfile)
            %loadAndConstructSimulation 
            %   Helper function that calls readStructFromXMLFile and constructor to create a new simulation object.
            setSimulation=mcmcInterface.simulation.readStructFromXMLFile(xmlfile);		
			retObj=mcmcInterface.simulation.constructor(setSimulation);
        end      
		
        function retObj=constructor(setSimulation)
            %constructor
            %   Static constructor that uses a structure with the settings to create a new simulation object.
            stdout=1;
    		fprintf(stdout, 'Constructing the simulation object\n');	
            
            setFileLog=cast(java.util.UUID.randomUUID.toString(),'char');
            setOsInterface=mcmcInterface.osInterfaces.constructor(setSimulation.osInterfaces);
            setSamplerInterface=mcmcInterface.samplerInterfaces.constructor(setSimulation.samplerInterfaces, setOsInterface);    
            setData=mcmcInterface.data.constructor(setSimulation.data);
            setVariables=mcmcInterface.variables.constructor(setSimulation.variables);
            
            fieldnameslist=fieldnames(setSimulation);
            if (any(contains(fieldnameslist,'predictions')))
                setPredictions=mcmcInterface.predictions.constructor(setSimulation.predictions);
            else
                setPredictions=mcmcInterface.predictions.empty;
            end
            %assumes we are making new samples (this is not supporting full load of an old samples file set)
            for chainid=1:setSamplerInterface.numSamplingChains
                tSamples=setSimulation.samples;%assumes just one <samples> entry loaded (not array from multiple chains) with dirSamples
                fieldnameslist=fieldnames(tSamples);
                if(~any(contains(fieldnameslist,'dirSamples')))
                    error('simulation constructor needs dirSamples for samples constructor');
                end                
                tSamples.uuid=setSimulation.uuid;
                tSamples.timeStamp=datestr(now,'HH:MM:SS.FFF');
                tSamples.chainid=chainid;
                tSamples.elements=setSimulation.variables.elements;
                tSamples.fileSamples=strcat(tSamples.dirSamples,mcmcInterface.osInterfaces.getNativeSeparator(),...
                    sprintf('%s-%s-chainid%d-samples.xml',setSimulation.fileNamePrefix,setSimulation.uuid,chainid));

                setSamples(chainid,1)=mcmcInterface.samples.constructor(tSamples);
            end

            retObj=mcmcInterface.simulation(setSimulation.uuid,setFileLog,setSimulation.fileNamePrefix,setSimulation.sampleResults,setSimulation.archiveSamples,setSimulation.enableDiagnosticPlots,setSimulation.enableDataPlots,setSimulation.enablePredictionPlots,setSimulation.savePlots,setSimulation.runView,...
                setOsInterface,setSamplerInterface,setData,setVariables,setSamples,setSimulation.dirSimulation,setSimulation.fileSimulation,setSimulation.generatePredictions,setSimulation.predictionStatisticsToCapture,setPredictions);
        end     		                      

        function retver=getVersion()%this allows us to read the private constant
            retver=mcmcInterface.simulation.version;
        end        
    end

    methods
        function obj = simulation(setUuid,setFileLog,setFileNamePrefix,setSampleResults,setArchiveSamples,setEnableDiagnosticPlots,setEnableDataPlots,setEnablePredictionPlots,setSavePlots,setRunView,setOsInterface,setSamplerInterface,setData,setVariables,setSamples,setDirSimulation,setFileSimulation,setGeneratePredictions,setPredictionStatisticsToCapture,setPredictions)
            %SIMULATION
            %   Constructor for simulation
             if nargin > 0
                obj.uuid=setUuid;
                obj.fileLog=setFileLog;
                obj.fileNamePrefix=setFileNamePrefix;
                obj.sampleResults=setSampleResults;
                obj.archiveSamples=setArchiveSamples;
                obj.enableDiagnosticPlots=setEnableDiagnosticPlots;
                obj.enableDataPlots=setEnableDataPlots;
                obj.enablePredictionPlots=setEnablePredictionPlots;
                obj.savePlots=setSavePlots;
                obj.runView=setRunView;                  
                obj.hOsInterfaces=setOsInterface;
                obj.hSamplerInterfaces=setSamplerInterface;
                obj.data=setData;
                obj.variables=setVariables;
                obj.samples=setSamples;
                obj.dirSimulation=setDirSimulation;
                obj.fileSimulation=setFileSimulation;
                obj.generatePredictions=setGeneratePredictions;
                obj.predictionStatisticsToCapture=setPredictionStatisticsToCapture;
                obj.predictions=setPredictions;
                %we don't instantiate hModel, it is passed in by runSimulation
             end
        end

        function retObj=transformPre(obj)
            %loop through the necessary variables and data elements to transform them for sampling
            %not needed on predictions, as samples are transformed prior to generating predictions

            retObj=obj;
            [retObj.variables,retObj.data]=retObj.hModel.transformPre(retObj.variables,retObj.data);
        end

        function retObj=transformPost(obj)
            %loop through the necessary samples and data elements to adjust them after sampling
            %not needed on predictions (see related comment for transformPre)
            %not needed on variables (i.e., initial values and hyperparameters that were altered via transformPre don't need to be transformed back to their initial values)

            retObj=obj;
            [retObj.samples,retObj.data]=retObj.hModel.transformPost(retObj.samples,retObj.data);
        end

        function retObj=runSimulationWorker(obj)
            %runSimulationWorker
            %   Worker function that runs the mcmc sampling session, reads the results, computes the statistics, displays the statistics, plots
            %   the diagnostics, and generates the samples output file
            stdout=1;

            retObj=obj.transformPre();

            if retObj.sampleResults %set to false to open existing sampler results file
                retObj.hSamplerInterfaces.setup(obj.hOsInterfaces, obj.uuid, obj.fileNamePrefix, obj.variables, obj.data);
                retObj.hSamplerInterfaces.run(obj.uuid, obj.fileNamePrefix, obj.runView);
            end
            for chainid=1:obj.hSamplerInterfaces.numSamplingChains
                fprintf(stdout,'\tProcessing chainid=%d\n',chainid);
                retObj.samples(chainid)=retObj.hSamplerInterfaces.read(obj.uuid, obj.fileNamePrefix, obj.variables, obj.samples, chainid);

                retObj=retObj.transformPost();
                
                retObj.samples(chainid)=retObj.samples(chainid).computeStatistics();
                retObj.samples(chainid).displayStatistics();
                if retObj.enableDiagnosticPlots %set to false to not plot diagnostics
                    retObj.samples(chainid).plotDiagnostics(obj.fileNamePrefix,retObj.savePlots);                    
                end
                if retObj.generatePredictions 
                    %call hModel to generate predictions
                    if isempty(retObj.predictions)
                        error('Unable to generate predictions because predictions data file is missing!')
                    end
                    retObj.predictions(chainid)=obj.hModel.generatePredictions(retObj.samples(chainid),retObj.predictions(chainid));
                    retObj.predictions(chainid)=obj.hModel.computePredictionStatistics(retObj.predictions(chainid));
                    if (obj.enableDataPlots) || (obj.enablePredictionPlots)
                        hPlot=obj.hModel.plotSetup(retObj.data, retObj.predictions(chainid));
                    end
                    if (obj.enableDataPlots)
                        obj.hModel.plotData(hPlot, retObj.data);
                    end
                    if (obj.enablePredictionPlots)
                        for i=1:size(obj.predictionStatisticsToCapture,1)%use the vector obj.predictionStatisticsToCapture to determine if retObj.data or retObj.predictions should be provided to plot
                            Yhattype=obj.predictionStatisticsToCapture(i);
                            obj.hModel.plotPredictions(hPlot, retObj.predictions(chainid), Yhattype);
                        end
                    end
                end
                samplesfile=strcat(retObj.samples(chainid).dirSamples,mcmcInterface.osInterfaces.getNativeSeparator(),...
                    sprintf('%s_%s_chainid%d_samples.xml',retObj.fileNamePrefix,retObj.uuid,chainid));
                if retObj.archiveSamples %set to false to not save samples to file
                    retObj.samples(chainid).writeSamplesToXML(samplesfile);%writes all sampling chains to samplesfile
                end
            end
        end

        function retObj=runSimulation(obj, hModel)
            %runSimulation
            %   Sets up and starts logging and then calls runSimulationWorker
            set(0,'DefaultFigureWindowStyle','docked')

            retObj=obj;
            retObj.hModel=hModel;

            %start logging
            retObj.fileLog=strcat(sprintf('%s',retObj.hOsInterfaces.dirLogs),mcmcInterface.osInterfaces.getNativeSeparator(),...
                sprintf('%s-sessionLog.txt',obj.fileLog));%fileLog has the logUUID previously generated
            logging.startLogging(retObj.fileLog);      
            
            retObj=retObj.runSimulationWorker();
            
            %stop logging and save
            permLog=strcat(sprintf('%s',retObj.hOsInterfaces.dirLogs),mcmcInterface.osInterfaces.getNativeSeparator(),...
                sprintf('%s-%s-sessionLog.txt',retObj.fileNamePrefix,retObj.uuid));            
            
            logging.stopAndSaveLog(retObj.fileLog,permLog);
            retObj.fileLog=permLog;
        end   

        function clearAll(obj)      
            obj.hOsInterfaces.clearAll();
            obj.hSamplerInterfaces.clearAll();
            chainid=1;%we only need one chainid to locate the samples directory, chainid=1 will always exist
            obj.samples(chainid).clearAll();%note that all chainid will be cleared
        end              
    end

    methods %getter/setter functions        
        function obj=set.variables(obj,setVariables) 
            if ~isa(setVariables,'mcmcInterface.variables')
                error('variables is not mcmcInterface.variables object');
            end
            obj.variables=setVariables;         
        end
        
        function obj=set.data(obj,setData) 
            if ~isa(setData,'mcmcInterface.data')
                error('data is not mcmcInterface.data object');
            end
            obj.data=setData;
        end

        function obj=set.samples(obj,setSamples) 
            if ~isa(setSamples,'mcmcInterface.samples')
                error('samples is not mcmcInterface.samples object');
            end
            obj.samples=setSamples;
        end

        function obj=set.hOsInterfaces(obj,sethOsInterfaces) 
            if ~isa(sethOsInterfaces,'mcmcInterface.osInterfaces')
                error('hOsInterfaces is not mcmcInterface.osInterfaces object');
            end
            obj.hOsInterfaces=sethOsInterfaces;
        end               

        function obj=set.hSamplerInterfaces(obj,sethSamplerInterfaces) 
            if ~isa(sethSamplerInterfaces,'mcmcInterface.samplerInterfaces')
                error('hSamplerInterfaces is not mcmcInterface.samplerInterfaces object');
            end
            obj.hSamplerInterfaces=sethSamplerInterfaces;
        end  
    end    
end

