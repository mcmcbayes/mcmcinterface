classdef statistics
    %STATISTICS 
    %   Class that stores statistics from samples.
    
    properties
        %statistics for samples
        median              %median of values (size depends on whether distribution is a scalar, vector, or matrix)
        sd                  %standard deviation of values (size depends on whether distribution is a scalar, vector, or matrix)
        min                 %minimum of values (size depends on whether distribution is a scalar, vector, or matrix)
        max                 %maximum of values (size depends on whether distribution is a scalar, vector, or matrix)
        hdrlo               %highest density region (a.k.a., highest posterior density for posterior distribution samples) lower limit (using hdralpha) (size depends on whether distribution is a scalar, vector, or matrix)
        hdrhi               %highest density region higher limit (using hdralpha) (size depends on whether distribution is a scalar, vector, or matrix) 
        mce                 %monte carlo error of the MCMC samples
        
        %statistics for samples and data
        mean                %mean of values (size depends on whether distribution is a scalar, vector, or matrix)

        %statistics for data
        hingelo             %low hinge (typically 95% of predictions lie above this)
        hingehi             %high hinge (typically 95% of predictions fall under this)

        %special case statistics
        histN               %histogram counts per bin
        histEdges           %edges that define the bins in the histogram
    end

    methods (Static)
        function setStatistics = readStructFromXMLNode(statisticsNode)
            %stdout=1;
            %fprintf(stdout, '\tLoading the statistics from statisticsNode\n');

            childNodeList=statisticsNode.getChildNodes();
            for i=1:childNodeList.getLength()
                tNode=childNodeList.item(i-1);
                if (strcmp("mean",tNode.getNodeName())==1)
                    setStatistics.mean=str2num(cast(tNode.getTextContent(),'char'));
                elseif (strcmp("median",tNode.getNodeName())==1)
                    setStatistics.median=str2num(cast(tNode.getTextContent(),'char'));
                elseif (strcmp("sd",tNode.getNodeName())==1)
                    setStatistics.sd=str2num(cast(tNode.getTextContent(),'char'));
                elseif (strcmp("min",tNode.getNodeName())==1)
                    setStatistics.min=str2num(cast(tNode.getTextContent(),'char'));
                elseif (strcmp("max",tNode.getNodeName())==1)
                    setStatistics.max=str2num(cast(tNode.getTextContent(),'char'));
                elseif (strcmp("hdrlo",tNode.getNodeName())==1)
                    setStatistics.hdrlo=str2num(cast(tNode.getTextContent(),'char'));
                elseif (strcmp("hdrhi",tNode.getNodeName())==1)
                    setStatistics.hdrhi=str2num(cast(tNode.getTextContent(),'char'));
                elseif (strcmp("mce",tNode.getNodeName())==1)
                    setStatistics.mce=str2num(cast(tNode.getTextContent(),'char'));
                end
            end
        end

        function retStatistics=constructor(setStatistics)
            %stdout=1;
    		%fprintf(stdout, 'Constructing the statistics object\n');
            retStatistics=mcmcInterface.statistics(setStatistics.mean,setStatistics.median,setStatistics.sd,setStatistics.min,setStatistics.max,setStatistics.hdrlo,setStatistics.hdrhi,setStatistics.mce);
        end          
    end

    methods
        function obj = statistics(setMean,setMedian,setSd,setMin,setMax,setHdrlo,setHdrhi,setMce)
            %STATISTICS 
            %   Construct an instance of this class
            obj.mean=setMean;
            obj.median=setMedian;
            obj.sd=setSd;
            obj.min=setMin;
            obj.max=setMax;
            obj.hdrlo=setHdrlo;
            obj.hdrhi=setHdrhi;
            obj.mce=setMce;
            obj.hingelo=[];%default is empty
            obj.hingehi=[];%default is empty
            obj.histN=[];%default is empty
            obj.histEdges=[];%default is empty
        end
        
        function retNode = createXmlNodeFromObject(obj,docNode,dimensions)
        %         <statistics><!--statistics on the samples-->
        %             <mean>[]</mean>
        %             <median>[]</median>
        %             <sd>[]</sd>
        %             <min>[]</min>
        %             <max>[]</max>
        %             <hdrlo>[]</hdrlo>
        %             <hdrhi>[]</hdrhi>
        %             <mce>[]</mce>
        %         </statistics>       

            retNode=docNode.createElement('statistics');

            tNode=docNode.createElement('mean');
            tNode.appendChild(docNode.createTextNode(mcmcInterface.values.toString(obj.mean,dimensions)));
            retNode.appendChild(tNode);     

            tNode=docNode.createElement('media');
            tNode.appendChild(docNode.createTextNode(mcmcInterface.values.toString(obj.median,dimensions)));
            retNode.appendChild(tNode);     

            tNode=docNode.createElement('sd');
            tNode.appendChild(docNode.createTextNode(mcmcInterface.values.toString(obj.sd,dimensions)));
            retNode.appendChild(tNode);          

            tNode=docNode.createElement('min');
            tNode.appendChild(docNode.createTextNode(mcmcInterface.values.toString(obj.min,dimensions)));
            retNode.appendChild(tNode);                 

            tNode=docNode.createElement('max');
            tNode.appendChild(docNode.createTextNode(mcmcInterface.values.toString(obj.max,dimensions)));
            retNode.appendChild(tNode);      

            tNode=docNode.createElement('hdrlo');
            tNode.appendChild(docNode.createTextNode(mcmcInterface.values.toString(obj.hdrlo,dimensions)));
            retNode.appendChild(tNode);     

            tNode=docNode.createElement('hdrhi');
            tNode.appendChild(docNode.createTextNode(mcmcInterface.values.toString(obj.hdrhi,dimensions)));
            retNode.appendChild(tNode);   

            tNode=docNode.createElement('mce');
            tNode.appendChild(docNode.createTextNode(mcmcInterface.values.toString(obj.mce,dimensions)));
            retNode.appendChild(tNode);                 
        end
    end
end

