classdef t_predictionStatistic < int32
    enumeration
        mean (1)
        hingelo (2)
        hingehi (3)
        histogram (4)   %normalized histogram
    end
    
    methods (Static)
        function value=fromstring(strvalue)
            if strcmp('mcmcInterface.t_predictionStatistic.mean',strvalue)==1
                value=mcmcInterface.t_predictionStatistic.mean;
            elseif strcmp('mcmcInterface.t_predictionStatistic.hingelo',strvalue)==1
                value=mcmcInterface.t_predictionStatistic.hingelo;
            elseif strcmp('mcmcInterface.t_predictionStatistic.hingehi',strvalue)==1
                value=mcmcInterface.t_predictionStatistic.hingehi;
            elseif strcmp('mcmcInterface.t_predictionStatistic.histogram',strvalue)==1
                value=mcmcInterface.t_predictionStatistic.histogram;
            else
                error('Unknown mcmcInterface.t_predictionStatistic');
            end
        end
    end    

    methods
        function strvalue=tostring(obj)
            switch obj
                case mcmcInterface.t_predictionStatistic.mean
                    strvalue='mcmcInterface.t_predictionStatistic.mean';
                case mcmcInterface.t_predictionStatistic.hingelo
                    strvalue='mcmcInterface.t_predictionStatistic.hingelo';  
                case mcmcInterface.t_predictionStatistic.hingehi
                    strvalue='mcmcInterface.t_predictionStatistic.hingehi';   
                case mcmcInterface.t_predictionStatistic.histogram
                    strvalue='mcmcInterface.t_predictionStatistic.histogram';   
               otherwise
                    error('unsupported mcmcInterface.t_predictionStatistic type');
            end
        end    
    end    
end

