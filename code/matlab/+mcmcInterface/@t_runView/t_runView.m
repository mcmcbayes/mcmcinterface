classdef t_runView < int32
    enumeration
        Background (1)
        Foreground (2)
    end
    
    methods (Static)       
        function enumobj=str2enum(strval)
            if strcmp('mcmcInterface.t_runView.Background',strval)==1
                enumobj=mcmcInterface.t_runView.Background;
            elseif strcmp('mcmcInterface.t_runView.Foreground',strval)==1
                enumobj=mcmcInterface.t_runView.Foreground;
            else
                error('unsupported mcmcInterface.t_runView type');
            end
        end        
    end

    methods
        function strvalue=tostring(obj)
            switch obj
                case mcmcInterface.t_runView.Background
                    strvalue='mcmcInterface.t_runView.Background';
                case mcmcInterface.t_runView.Foreground
                    strvalue='mcmcInterface.t_runView.Foreground';  
                otherwise
                    error('unsupported mcmcInterface.t_runView type');
            end
        end    
    end
end

