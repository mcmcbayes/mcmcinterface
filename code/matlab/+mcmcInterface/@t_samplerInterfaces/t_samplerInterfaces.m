classdef t_samplerInterfaces < int32
    enumeration
        OpenBUGS (1)
        JAGS (2)
        Stan (3)
    end
    
    methods (Static)
        function enumobj=str2enum(strval)
            if strcmp('mcmcInterface.t_samplerInterfaces.OpenBUGS',strval)==1
                enumobj=mcmcInterface.t_samplerInterfaces.OpenBUGS;
            elseif strcmp('mcmcInterface.t_samplerInterfaces.JAGS',strval)==1
                enumobj=mcmcInterface.t_samplerInterfaces.JAGS;
            elseif strcmp('mcmcInterface.t_samplerInterfaces.Stan',strval)==1
                enumobj=mcmcInterface.t_samplerInterfaces.Stan;
            else
                error('unsupported mcmcInterface.t_samplerInterfaces type');
            end
        end           
    end

    methods
        function strvalue=tostring(obj)
            switch obj
                case mcmcInterface.t_samplerInterfaces.OpenBUGS
                    strvalue='mcmcInterface.t_samplerInterfaces.OpenBUGS';
                case mcmcInterface.t_samplerInterfaces.JAGS
                    strvalue='mcmcInterface.t_samplerInterfaces.JAGS';
                case mcmcInterface.t_samplerInterfaces.Stan
                    strvalue='mcmcInterface.t_samplerInterfaces.Stan';
                otherwise
                    error('unsupported mcmcInterface.t_samplerInterfaces type');
            end
        end
    end
end

