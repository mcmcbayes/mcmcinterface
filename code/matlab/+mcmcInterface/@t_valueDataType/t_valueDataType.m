classdef t_valueDataType < int32
    enumeration
        constant (1)    %constants, hyperparameters, fixed data, etc.
        generated (2)   %samples, generated random variables, predictions, etc.
    end
    methods (Static)        
        function enumobj=string2enum(strval)
            if strcmp(strval,'mcmcInterface.t_valueDataType.constant')==1
                enumobj=mcmcInterface.t_valueDataType.constant;
            elseif strcmp(strval,'mcmcInterface.t_valueDataType.generated')==1
                enumobj=mcmcInterface.t_valueDataType.generated;
            else
                error('incorrect type')
            end
        end
        
        function strvalue=tostring(enumobj)
            switch enumobj
                case mcmcInterface.t_valueDataType.constant
                    strvalue='mcmcInterface.t_valueDataType.constant';
                case mcmcInterface.t_valueDataType.generated
                    strvalue='mcmcInterface.t_valueDataType.generated';
                otherwise
                    error('incorrect type')
            end
        end                
    end
end

