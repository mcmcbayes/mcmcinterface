classdef t_valuesDimensions < int32
    enumeration
        scalar (1)
        vector (2)
        matrix2D (3)
    end
    
    methods (Static)        
        function enumobj=str2enum(strval)
            if strcmp('mcmcInterface.t_valuesDimensions.scalar',strval)==1
                enumobj=mcmcInterface.t_valuesDimensions.scalar;
            elseif strcmp('mcmcInterface.t_valuesDimensions.vector',strval)==1
                enumobj=mcmcInterface.t_valuesDimensions.vector;
            elseif strcmp('mcmcInterface.t_valuesDimensions.matrix2D',strval)==1
                enumobj=mcmcInterface.t_valuesDimensions.matrix2D;
            else
                error('unsupported mcmcInterface.t_valuesDimensions type');
            end
        end
    end

    methods
        function strvalue=tostring(obj)
            switch obj
                case mcmcInterface.t_valuesDimensions.scalar
                    strvalue='mcmcInterface.t_valuesDimensions.scalar';
                case mcmcInterface.t_valuesDimensions.vector
                    strvalue='mcmcInterface.t_valuesDimensions.vector';
                case mcmcInterface.t_valuesDimensions.matrix2D
                    strvalue='mcmcInterface.t_valuesDimensions.matrix2D';
                otherwise
                    error('unsupported mcmcInterface.t_valuesDimensions type');
            end
        end    
    end
end