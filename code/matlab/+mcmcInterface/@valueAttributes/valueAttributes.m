classdef valueAttributes < handle
    %VALUEATTRIBUTES Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        dimensions  %t_valuesDimensions.{scalar,vector,matrix2D}
        type        %strings describing the matlab data type (e.g., 'int32', 'double', 'categorical')
        lowerBound  %lower numerical limit
        upperBound  %upper numerical limit
        precision   %number of decimal places to display
    end

    methods
        function obj = valueAttributes(setDimensions,setType,setLowerBound,setUpperBound,setPrecision)
            %VALUEATTRIBUTES Construct an instance of this class
            %   Detailed explanation goes here
            obj.dimensions=setDimensions;
            obj.type=setType;
            obj.lowerBound=setLowerBound;
            obj.upperBound=setUpperBound;
            obj.precision=setPrecision;
        end
    end

    methods %getter/setter functions
        function set.dimensions(obj,setDimensions) 
            if ~isa(setDimensions,'mcmcInterface.t_valuesDimensions')
                error('dimensions is not mcmcInterface.t_valuesDimensions object');
            end
            obj.dimensions=setDimensions;
        end

        function set.type(obj,setType) 
            if ((strcmp("double",setType)==1) || ...
                (strcmp("int32",setType)==1) || ...
                (strcmp("categorical",setType)==1))
                obj.type=setType;
            else
                error('type is not double, int32, or categorical');
            end
        end
    end    
end

