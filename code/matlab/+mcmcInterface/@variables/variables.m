classdef variables
    %VARIABLES
    %   This class, mcmcInterface.variables, is a data structure used to contain variables and constants 
    % (e.g., hyperparameters that define mode variable prior distributions) for the simulation.  
    
    properties (Constant, Access = private) %define as private so that subclasses can have their own instances with same name
        version='1.0';      % API version for this class
    end

    properties
        uuid                % unique identifier for the variables set
        description         % description of the variables set
        author              % creator of the variables set file
        reference           % citations and useful sources
        elements            % array of element(numVar) objects with the variables information
        dirVariables        % directory that contains variables *.xml file to load
        fileVariables       % filename of the variables *.xml file to load
    end

    methods (Static)
        function setVariables=readStructFromXMLFile(xmlfile, myrepos)        
            %readStructFromXMLFile 
            %   Load a simulation object from *.xml.
            % xmlfile-simulation xmlfile name (either absolute or relative path inside repos's config directory)
            % myrepos (optional)-myrepos object with base dir and list of repositories
            stdout=1;
            fprintf(stdout, 'Loading %s for the variables\n',xmlfile);
            
            doc = xmlread(xmlfile);
            %fprintf(1,'Parsed %s\n',xmlfile);
            %javaaddpath(mcmcBayes.osInterfaces.getPath(mcmcBayes.t_paths.javaDir));
            %call our java hacks method to insert the DOCTYPE element
            entityname='variables';
            %     xmlStr=XmlHacks.insert(doc, entityname, dtdfile);%call Java function  (XmlHacks must be on javaclasspath)
            %     %now parse the hacked xml
            %     doc = XmlHacks.xmlreadstring(xmlStr);
            
            doc.getDocumentElement().normalize();
            rootNode=doc.getDocumentElement();
            rootname=rootNode.getNodeName();
            if ~(strcmp(rootname,entityname)==1)
                error('Root element should be entityname');
            end
            
            setVariables=mcmcInterface.variables.readStructFromXMLNode(rootNode, myrepos);             
        end

        function setVariables=readStructFromXMLNode(rootNode, myrepos)        
            %readStructFromXMLNode
            %   Worker function that parses and returns information within a structure that represents a root element.  Function can also be called
            %with a sparse structure, that only contains some (and not all) fields in the root element (so this function must handle missing entries).
            % rootNode-data structure with the xml node read in via xmlread
            % myrepos (optional)-myrepos object with base dir and list of repositories

            stdout=1;
            fprintf(stdout, '\tLoading the variables from rootNode\n');

            %check version attribute
            tversion=cast(rootNode.getAttribute('version'),'char');
            if (strcmp(mcmcInterface.variables.getVersion(),tversion)~=1)
                error('variables version does not match in *.xml');
            end

            %get the root attributes (if they exist)
            tattributes=rootNode.getAttributes();
            for i=1:tattributes.getLength()
                attributeName=cast(tattributes.item(i-1).getName(),'char');
                if (strcmp(attributeName,'uuid')==1)
                    setVariables.uuid=cast(tattributes.item(i-1).getTextContent(),'char');
                elseif (strcmp(attributeName,'description')==1)
                    setVariables.description=cast(tattributes.item(i-1).getTextContent(),'char');
                elseif (strcmp(attributeName,'author')==1)
                    setVariables.author=cast(tattributes.item(i-1).getTextContent(),'char');
                elseif (strcmp(attributeName,'reference')==1)
                    setVariables.reference=cast(tattributes.item(i-1).getTextContent(),'char');
                end
            end

            loadStatus.dirVariables=false;
            loadStatus.fileVariables=false;
            numElements=0;
            for i=1:rootNode.getLength()
                tNode=rootNode.item(i-1);
                if (strcmp(tNode.getNodeName,'dir')==1)
                    tDir=mcmcInterface.storageObjectPath(tNode,myrepos);
                    if (strcmp(tDir.name,'dirVariables')==1)
                        loadStatus.dirVariables=true;
                        setVariables.dirVariables=tDir.path;
                    else
                        error('unexpected xml node name encountered for dir');
                    end
                elseif (strcmp(tNode.getNodeName,'file')==1)
                    tFile=mcmcInterface.storageObjectPath(tNode,myrepos);
                    if (strcmp(tFile.name,'fileVariables')==1)
                        if (loadStatus.dirVariables==false)
                            error('need to have dirVariables in xml');
                        end
                        loadStatus.fileVariables=true;
                        setVariables.fileVariables=tFile.path;
                        tfilename=strcat(setVariables.dirVariables,mcmcInterface.osInterfaces.getNativeSeparator(),setVariables.fileVariables);
                        setVariablesOverload=mcmcInterface.variables.readStructFromXMLFile(tfilename, myrepos);
                        setVariables=mcmcInterface.variables.updateStruct(setVariables,setVariablesOverload);
                    else
                        error('unexpected xml node name encountered for file');
                    end
                elseif (strcmp(tNode.getNodeName,'element')==1)
                    numElements=numElements+1;
                    setVariables.elements(numElements,1)=mcmcInterface.element.readStructFromXMLNode(tNode);
                end
            end
        end

        function setVariables=updateStruct(setVariablesOriginal,setVariablesOverload)  
            stdout=1;
            fprintf(stdout, '\tOverloading fields in variables\n');

            setVariables=setVariablesOriginal;
            overloadfieldnameslist=fieldnames(setVariablesOverload);
            if(any(contains(overloadfieldnameslist,'uuid')))
                setVariables.uuid=setVariablesOverload.uuid;
            end
            if(any(contains(overloadfieldnameslist,'description')))
                setVariables.description=setVariablesOverload.description;
            end    
            if(any(contains(overloadfieldnameslist,'author')))
                setVariables.author=setVariablesOverload.author;
            end
            if(any(contains(overloadfieldnameslist,'reference')))
                setVariables.reference=setVariablesOverload.reference;
            end
            if(any(contains(overloadfieldnameslist,'elements')))
                setVariables.elements=setVariablesOverload.elements;
            end            
            if(any(contains(overloadfieldnameslist,'dirVariables')))
                setVariables.dirVariables=setVariablesOverload.dirVariables;
            end
            if(any(contains(overloadfieldnameslist,'fileVariables')))
                setVariables.fileVariables=setVariablesOverload.fileVariables;
            end
        end      

        function retVariables=loadAndConstructVariables(xmlfile)
            setVariables=mcmcInterface.variables.readStructFromXMLFile(xmlfile);		
            retVariables=mcmcInterface.variables.constructor(setVariables);
        end

        function retVariables=constructor(setVariables)
            stdout=1;
    		fprintf(stdout, 'Constructing the variables object\n');
            for i=1:size(setVariables.elements,1)
                setElements(i,1)=mcmcInterface.element.constructor(setVariables.elements(i));
            end            
            retVariables=mcmcInterface.variables(setVariables.uuid,setVariables.description,setVariables.author,setVariables.reference,setElements,setVariables.dirVariables,setVariables.fileVariables);
        end            

        function retver=getVersion()%this allows us to read the private constant
            retver=mcmcInterface.variables.version;
        end        
    end
    
    methods
        function obj = variables(setUuid,setDescription,setAuthor,setReference,setElements,setDirVariables,setFileVariables)
            %variables
            %   Construct an instance of this class
            if nargin>0
                obj.uuid=setUuid;
                obj.description=setDescription;
                obj.author=setAuthor;
                obj.reference=setReference;
                obj.elements=setElements;
                obj.dirVariables=setDirVariables;
                obj.fileVariables=setFileVariables;
            end
        end

        function quickLook(obj)
            stdout=1;
            fprintf(stdout,'\tVariables=%s/%s\n',obj.dirVariables,obj.fileVariables);
            numElements=size(obj.elements,1);
            for i=1:numElements
                fprintf(stdout,'\t\t');
                obj.elements(i).quickLook()
            end
        end
    end

    methods %getter/setter functions
        function obj=set.elements(obj,setElements) 
            if ~isa(setElements,'mcmcInterface.element')
                error('elements is not mcmcInterface.element object');
            end
            obj.elements=setElements;
        end    
    end    
end

