function mat2rdump(varargin)
%MAT2RDUMP - save matlab variables in dump format for JAGS
% This code was not readable so I refined a significant portion of it, including the api! RAJ
%
% This function is essentially what was originally provided by MAT2BUGS.  Unfortunately, the documentation for R's dump()
% is non-existent (thanks!) and I can't verify my changes. I replaced the '=' with '->' and removed the exterior wrapper
% 'list(%CONTENTS%)' (i.e., 'list(' and ')' are removed and only the %CONTENTS% remain. Format for matrices is also
% slightly different between BUGS/JAGS (see the next comments).
% (reuben@reubenjohnston.com)
%
% Reference: http://staffblogs.le.ac.uk/bayeswithstata/2014/11/21/jags-with-stata/
% The Data & Initial Values File
%
% "One of the ways in which JAGS is geared to work with R is that JAGS formats its data files in the layout provided by R's dump() function. 
% Here is a piece of R code that demonstrates the format:"
% > x <- c(1,2,3,4)
% > p <- 5
% > m <- matrix(c(1,2,3,4,5,6),2,3)
% > m
%          [,1] [,2] [,3]
%          [1,] 1 3 5
%          [2,] 2 4 6
% > dump(c("x","p","m"),file="text.txt")
% > file.show("text.txt")
%          x <-
%          c(1, 2, 3, 4)
%          p <-
%          5
%          m <-
%          structure(c(1, 2, 3, 4, 5, 6), .Dim = 2:3)
%
%mat2rdump(filename, [{name1} {value1}; {name2} {value2} , ... ])
%  Writes S-PLUS data specification to the standard output for given variable names and values.
%  Any number of name,value pairs can be given in the array.
%
%Examples:
%  mat2jags('jags_data.dat', [{'x'}, {x}; {'y'}, {y}])
%
%BUGS: (no pun intended)
%  Supports only scalars and 2D tables.  Also, 1D tables are converted to 2d (i.e., a 1xN). 
%
% (c) jouko.lampinen@hut.fi, 1998-1999
% 2002-11-01 Aki.Vehtari@hut.fi - Added handling for NaN
% 2003-01-14 Aki.Vehtari@hut.fi - Option to force use of 1D and 2D tables

%input validation
if ischar(varargin{1})
    ofile=fopen(varargin{1},'w');
    if ofile<1
        error(['Cannot open ' varargin{1}]); 
    end
    i=1;
else
    error('There was an issue with creating/opening the data file for the sampler');
end

%input validation
if size(varargin,2)~=2
    error('You passed in an empty data list');
end
nargin=size(varargin{2},1);

while i<=nargin
    name=varargin{2}{i,1};
    val=varargin{2}{i,2};
    defdims=varargin{2}{i,3};%base dimensions alternatives: mcmcBayes.t_numericDimensions{scalar,vector,matrix2D}    
    [dims]=size(val);%for Tau samples, varsize translates into: Tau x 1 (scalar); R x Tau (vector); R x C x Tau (matrix)
    ROW=dims(1);%rows (this is not the same as the variable R)
    COL=dims(2);%columns (this is not the same as the variable C)
    if defdims==mcmcInterface.t_valuesDimensions.matrix2D
        error('Unsupported matrix size');
    elseif defdims==mcmcInterface.t_valuesDimensions.scalar && ROW==1
        fprintf(ofile,'''%s''<-',name);
        fprintval(ofile,val);
    else %it is a vector or a 2x2 matrix, just use a structure for either
        fprintf(ofile,'''%s''<-structure(\n c(\t',name);
        %structure indexing is wonky, see the documentation above
        if ROW==0%empty value case
            fprintf(ofile,'),\n');
        else
            for col=1:COL
                if col>1
                    fprintf(ofile,'\t');
                end
                for row=1:ROW
                    fprintval(ofile,val(row,col));
                    if row==ROW
                        if col<COL
                            fprintf(ofile,',\n');
                        else
                            fprintf(ofile,'),\n');
                        end
                    else
                        fprintf(ofile,', ');                       
                    end
                end         
            end
        end
        fprintf(ofile,'  .Dim=c(%G,%G))\n',ROW,COL);
    end
    if i<nargin
        fprintf(ofile,'\n');
        i=i+1;
    else
        i=i+1;
    end
end
fprintf(ofile,'\n');
if ofile>2
    fclose(ofile);
end

function fprintval(ofile, val)
if iscategorical(val)
    fprintf(ofile,'%G',val);
elseif isnan(val)
    fprintf(ofile,'NA');
elseif isinteger(val)
    fprintf(ofile,'%d',val);
else
    tempstr=sprintf('%E',val);
    if strfind(tempstr,'E+0')>0
        tempstr=strrep(tempstr,'E+0','E+');
    elseif strfind(tempstr,'E-0')>0
        tempstr=strrep(tempstr,'E-0','E-');
    end
    if isempty(strfind(tempstr,'.')) & (strfind(tempstr,'E')>0) %todo: this is bad code
        tempstr=strrep(tempstr,'E','.0E');
    end
    fprintf(ofile,'%s',  tempstr);
end
