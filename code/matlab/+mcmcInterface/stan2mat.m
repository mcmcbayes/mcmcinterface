function S=stan2mat(variables,filename)
    stdout=1;
    %fprintf(stdout,'Reading in %s and removing comments.\n',filename);

    fid=fopen(filename,'r');
    assert(fid>=0, 'Problem opening the file (ie., it does not exist ... perhaps you have cfg_mcmcBayesSampleMCMCResults turned off and cleared the old MCMC data file?)');
    strbuffer=fscanf(fid,'%c');
    fclose(fid);
    delete(filename);
    fid=fopen(filename,'w');%don't use 't' (for text) so that we can control line endings to be unix  
    strlines=strsplit(strbuffer,'\n');
    for strline=strlines
        if ~(strncmp('#',cell2mat(strline),1)==1), fprintf(fid,'%s\n', cell2mat(strline)); end;%need the '\n'
    end

    fclose(fid);
    
    fprintf(stdout,'\tImporting the csv formatted table data from %s into samples structure.\n', filename);
    warning('OFF', 'MATLAB:table:ModifiedAndSavedVarnames');%suppress this warning
    xlTable=readtable(filename);%read the samples file into a table
    
    %Examples of Stan vars are a_1.1.1,a_1.2.1,a_1.3.1,a_1.4.1,a_1.5.1,a_2.1.1,a_2.2.1,a_2.3.1,a_2.4.1,a_2.5.1,a_3.1.1,a_3.2.1,a_3.3.1,a_3.4.1,a_3.5.1,a_4.1.1,a_4.2.1,a_4.3.1,a_4.4.1,a_4.5.1,sigma
    %MATLAB readtable will convert the Stan var names above by replacing '.' with '_' (e.g., 'a_1.1.1' becomes --> 'a_1_1_1')
    %Need to translate corresponding varNames from variables (e.g., a_1, a_2, a_3, a_4, r) into Stan vars above, and then reshape the Stan vars into the appropriate sizes
    for varid=1:size(variables.elements,1)
        if variables.elements(varid).distribution.type~=mcmcInterface.t_elementDistribution.fixed
            tvarlist={};
            varnamePrefix=variables.elements(varid).name;
            varSizes=size(variables.elements(varid).values.valueConstant.data);
            if variables.elements(varid).values.hValueAttributes.dimensions==mcmcInterface.t_valuesDimensions.scalar
                tvarlist=[{varnamePrefix}];
            elseif variables.elements(varid).values.hValueAttributes.dimensions==mcmcInterface.t_valuesDimensions.vector                
                C=varSizes(2);
                for col=1:C
                    tvarname=sprintf('%s_1_%d',varnamePrefix,col);
                    if isempty(tvarlist)%tvarlist must be a cell array
                        tvarlist=[{tvarname}];
                    else
                        tvarlist=[tvarlist {tvarname}];
                    end
                end
            else%2d-matrix
                error('todo');
            end                        
            
            if ~exist('varNames','var')%tvarlist must be a cell array
                varNames=[tvarlist];
            else
                varNames=[varNames tvarlist];
            end
        end
    end
    
    %trim the table columns
    xlTable=xlTable(:,varNames);
    for varName=varNames
        eval(['samples=xlTable.' cell2mat(varName) ';']);
        eval(['tS.' cell2mat(varName) '=samples;']);
    end
    
    %now need to reshape the samples
    for varid=1:size(variables.elements,1)
        if variables.elements(varid).distribution.type~=mcmcInterface.t_elementDistribution.fixed
            varnamePrefix=variables.elements(varid).name;
            varSizes=size(variables.elements(varid).values.valueConstant.data);
            if variables.elements(varid).values.hValueAttributes.dimensions==mcmcInterface.t_valuesDimensions.scalar
                eval(['S.' varnamePrefix '=tS.' varnamePrefix ';']);
            elseif variables.elements(varid).values.hValueAttributes.dimensions==mcmcInterface.t_valuesDimensions.vector
                M=varSizes(2);
                for col=1:M
                    tvarname=sprintf('%s_1_%d',varnamePrefix,col);
                    evalstr=sprintf('S.%s(:,%d)=tS.%s;',varnamePrefix,col,tvarname);
                    eval(evalstr);
                end
            else%2d-matrix
                error('todo');
            end
        end
    end    
    %fprintf(stdout,'Import completed.\n');
end