%filename-file to insert the buffer 
%buffer-string to insert
function fwritebuffer(filename, cellbuffer)
fileID = fopen(filename,'w');
assert(fileID>=0, 'Problem opening the file');

numline=numel(cellbuffer);

cellbuffer=cellbuffer';
%write cell array cellbuffer into file line by line
for index=1:numline
    fprintf(fileID,'%s\n',char(cellbuffer{index}));  
end
fclose(fileID);