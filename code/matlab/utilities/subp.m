function [subax]=subp(m, n, p)%plot figures only when Desktop is not minimized
    dtframe = com.mathworks.mlservices.MatlabDesktopServices.getDesktop.getMainFrame;
    if isempty(dtframe)
        setVisibility('Off');%hide  all figures
        set(groot, 'DefaultFigureVisible', 'Off');%default always off, needs to be first
    else
        if (get(dtframe,'Minimized')==1) || ~(usejava('Desktop'))%needs to happen before the figure manipulation below
            setVisibility('Off');%hide  all figures
            set(groot, 'DefaultFigureVisible', 'Off');%default always off, needs to be first
        else
            setVisibility('On');%show all figures
            set(groot, 'DefaultFigureVisible', 'On');%default always off, needs to be first
        end
    end
    
    subax=subplot(m,n,p);
end